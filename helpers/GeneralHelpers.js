import moment from 'moment'
export function replaceImgUrl(url) {
    if(url.includes("https://www.cryptoknowmics.com/news/")){
        return url.replace("https://www.cryptoknowmics.com/news/", "https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/crypto/");
    }else{
        return url.replace("https://www.cryptoknowmics.com/crypto/", "https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/crypto/");
    }  
};


export function newsUrl(slug) {
    return `/news/${slug}`;
}


export function replaceHtml(text,langth = text.length) {
    const regex = /(<([^>]+)>)/ig;
    // let content;
    return langth ? text.replace(regex, '').substring(0, langth) : text.replace(regex, '')
    
};

export function makeSlug(url){
    
     url = url.replace(/[&\\\#, +()$~%.'":*?<>{}]/g, '-');
     url = url.toLowerCase();
     return url.replace(/--/g, "-"); // return url.replace(/[^a-zA-Z0-9]/g, '');
}

export function shortText(text, length) {
    return text.substring(0, length);
}


export function dateFormat(date, type){
    let data;
    type == "date" ?
    data = moment(date).format("MMM D, YYYY")
    : type == "data_and_time" ? data =  moment(date).format("MMM D, YYYY, h:mm a") 
    : data =  moment(date).format("MMM D, YYYY, h:mm a")

    return data;
}