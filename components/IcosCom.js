import React from 'react'
import Image from 'next/image';
import Link from 'next/link';
import { dateFormat, makeSlug, replaceHtml, replaceImgUrl, shortText } from '../helpers/GeneralHelpers';

const IcosCom = (props) => {
    const {pro_data} = props
    return (
            
            <div className={`row wcolor align-items-center lborder mx-0 ${pro_data.id === pro_data.arr_length - 1 ? '' : 'border-bottom mb-3'}`}>
                <div className="col-md-8 ps-0">
                    <div className="row">
                        <div className="col-md-2 me-0">
                            <Link href={pro_data.slug}>
                                <a>
                                    <Image
                                        src={pro_data.image}
                                        width={80}
                                        height={80}
                                        webp="true"
                                        className="rounded"
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className="col-md-10 ps-0">
                            <div className="pe-5">
                                <Link href={pro_data.slug}>
                                    <a className="dwcolor">{pro_data.title}</a>
                                </Link>
                                <p>{`${pro_data.description.length > 120 ? replaceHtml(shortText(pro_data.description,120))+' ...' : replaceHtml(shortText(pro_data.description,120))}`}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-2"><span>{dateFormat(pro_data.start,'date')}</span></div>
                <div className="col-md-2 pe-0"><span>{dateFormat(pro_data.end,'date')}</span></div>
            </div>
    )
}

export default IcosCom
