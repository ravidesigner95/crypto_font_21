import Image from 'next/image';
import Link from 'next/link';
import {replaceHtml} from '../../helpers/GeneralHelpers'
export default function SmTeam(props) {
    return (
        <>
            <div className="box text-center inShadowBox position-relative p-3 py-5 rounded">
                <a href={(props.item.linkedin!= '')?props.item.linkedin:'#'} className="fab fa-linkedin"></a>
                <a href={(props.item.twitterk!= '')?props.item.twitterk:'#'} className="fab fa-twitter"></a>
                <Link href={(props.item.slug != "")?`/news/author/${props.item.slug}`:"#"}>
                <a className="d-block">
                    <div className="img">
                        <Image src={props.item.image}
                            alt={props.item.name}
                            layout="responsive"
                            width={90}
                            height={90}
                            webp="true"/>
                    </div>
                    <h6 className="text-white rounded mt-4 mb-3">{props.item.name}</h6>
                    {(() => {
                        if(props.item.position != ""){
                            <div className="inShadowBox py-2 rounded bcolor mb-3">{props.item.position}</div>
                        }
                    })()}
                    <p className="wcolor text-center mb-0">{replaceHtml(props.item.description)}</p>
                </a>
                </Link>
            </div>
        </>
    )
}