import Link from 'next/link';
import Image from 'next/image';

import {replaceHtml, replaceImgUrl, newsUrl, dateFormat} from '../../helpers/GeneralHelpers';


export default function SmNews(props) {
    return (
        <> {" "}
            {
            (() => {
                if (props.type == 1) {
                    return (
                        <div className="col-md-4 mb-5">
                            <div className="box p-3 outShadowBox rounded post_box h-100">
                                <div className="img">
                                    <Link href={
                                        newsUrl(props.news.slug)
                                    }>
                                        <a>
                                            <Image src={
                                                    replaceImgUrl(props.news.image)
                                                }
                                                alt={
                                                    props.news.post_title
                                                }
                                                layout="responsive"
                                                width={300}
                                                height={190}
                                                webp="true"
                                                className="w-100 rounded"/>
                                        </a>
                                    </Link>
                                </div>
                                <div className="text_body">
                                    <h6 className="m-0 py-3">
                                        <Link href={
                                            newsUrl(props.news.slug)
                                        }>
                                            <a className="text-white text-decoration-none title">
                                                {
                                                props.news.post_title
                                            } </a>
                                        </Link>
                                    </h6>
                                    <p className="lgcolor">
                                        {
                                        replaceHtml(props.news.post_content)
                                    } </p>
                                    <div className="text-end">
                                        <small className="text-uppercase text-white">
                                            {
                                            dateFormat(props.news.post_date)
                                        } </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                } else if (props.type == 2) {
                    return (
                        <li className="">
                            <Link href={
                                newsUrl(props.news.slug)
                            }>
                                <a>
                                    <Image src={
                                            replaceImgUrl(props.news.image)
                                        }
                                        alt={
                                            props.news.post_title
                                        }
                                        // layout="responsive"
                                        width={120}
                                        height={80}
                                        webp="true"/>
                                    <div className="ms-3">
                                        <h6>{
                                            props.news.post_title
                                        }</h6>
                                        <small>{
                                            dateFormat(props.news.post_date)
                                        }</small>
                                    </div>
                                </a>
                            </Link>
                        </li>
                    );
                } else if (props.type == 3) {
                    return (
                        <div className="col-md-8">
                            <div className="p-3 outShadowBox rounded big_post">
                                <div className="box">
                                    <Link href={
                                        newsUrl(props.news.slug)
                                    }>
                                        <a className="d-block position-relative text-white">
                                            {/* <img src="/images/slider1.jpg" className="d-block w-100" alt="images/slider1.jpg"/> */}
                                            <Image src={
                                                    replaceImgUrl(props.news.image)
                                                }
                                                alt={
                                                    props.news.post_title
                                                }
                                                layout="responsive"
                                                width={750}
                                                height={468}
                                                className="d-block w-100"
                                                webp="true"/>
                                            <div className="text_box position-absolute bottom-0 start-0">
                                                <h5>{
                                                    props.news.post_title
                                                }</h5>
                                                <p className="">
                                                    {
                                                    `${props.news.post_content.length > 170 ? replaceHtml(props.news.post_content,170)+' ...' : replaceHtml(props.news.post_content)}`
                                                } </p>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <strong className="py-1 px-3 rounded-2 bbgcolor">
                                                        <small className=" text-uppercase">
                                                            {
                                                            props.news.category_name
                                                        } </small>
                                                    </strong>
                                                    <small className="text-uppercase">
                                                        {
                                                        dateFormat(props.news.post_date)
                                                    } </small>
                                                </div>
                                            </div>
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    );
                } else if (props.type == 4) {
                    return (
                        <div class="col-md-6 mb-4">
                            <div class="ver_post_box rounded inShadowBox p-3">
                                <div class="row">
                                    <div class="col-md-5">
                                        <Link href={`/news/${props.news.slug}`}
                                       >
                                            <a  onClick={props.getDet}
                                        slug={props.news.slug}>
                                                <Image src={
                                                        `https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/crypto/wp-content/uploads/${props.news.image}`
                                                    }
                                                    alt={
                                                        props.news.post_title
                                                    }
                                                    layout="responsive"
                                                    width={750}
                                                    height={468}
                                                    className="d-block w-100"
                                                    webp="true"/>
                                            </a>
                                        </Link>
                                    </div>
                                    <div class="col-md-7">
                                        <h6 class="mb-3">
                                            <Link href={`/news/${props.news.slug}`}>
                                                <a class="text-white title"  onClick={props.getDet}
                                        slug={props.news.slug}>
                                                    {
                                                    props.news.post_title
                                                } </a>
                                            </Link>
                                        </h6>
                                        <p class="py-2 border-top border-bottom lborder mb-3">
                                            {
                                            replaceHtml(props.news.post_content)
                                        }</p>
                                        <div class="d-flex">
                                            {/* <strong>
                                                <a href="#" class="text-white"></a>
                                            </strong> */}
                                            <span class="lgcolor">&#160;&#160;|&#160;&#160;{
                                                dateFormat(props.news.post_date)
                                            }</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }else if (props.type == 5) {
                    return (
                        <div class="col-md-6 mb-4">
                            <div class="ver_post_box rounded inShadowBox p-3">
                                <div class="row">
                                    <div class="col-md-5">
                                        <Link href={`/news/${props.news.slug}`}
                                       >
                                            <a  onClick={props.getDet}
                                        slug={props.news.slug}>
                                                <Image src={replaceImgUrl(props.news.image)}
                                                    alt={
                                                        props.news.post_title
                                                    }
                                                    layout="responsive"
                                                    width={750}
                                                    height={468}
                                                    className="d-block w-100"
                                                    webp="true"/>
                                            </a>
                                        </Link>
                                    </div>
                                    <div class="col-md-7">
                                        <h6 class="mb-3">
                                            <Link href={`/news/${props.news.slug}`}>
                                                <a class="text-white title"  onClick={props.getDet}
                                        slug={props.news.slug}>
                                                    {
                                                    props.news.post_title
                                                } </a>
                                            </Link>
                                        </h6>
                                        <p class="py-2 border-top border-bottom lborder mb-3">
                                            {
                                            replaceHtml(props.news.post_content)
                                        }</p>
                                        <div class="d-flex">
                                            {/* <strong>
                                                <a href="#" class="text-white"></a>
                                            </strong> */}
                                            <span class="lgcolor">&#160;&#160;|&#160;&#160;{
                                                dateFormat(props.news.post_date, 1)
                                            }</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }
                
            })()
        }
            {" "} </>
    );
}