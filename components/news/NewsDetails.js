import Link from 'next/link';
import {useState, useEffect} from "react";
import Image from 'next/image';
import ReactHtmlParser from 'react-html-parser';
import {replaceHtml, replaceImgUrl, newsUrl, dateFormat} from '../../helpers/GeneralHelpers';
export default function newsdetails(props){
    
    let newsContent = props.details[3][0].post_content;
    newsContent = newsContent.replace(/src=\"(.+?)\"/g, function (m, p1) {
        return `src="${replaceImgUrl(p1)}"`;;
    });
    newsContent = newsContent.split("\r\n");
    let tagSlug = props.details[6][0].Tags_slug.split(", ");
    useEffect(() => {
        // console.log(props);
        console.log(props.getDet);
    })
    return(
        <>
        <section className="pt-4 pb-5">
                    <div className="container-fluid px-5">
                        <div className="row">
                            <div className="col-md-7">
                                <div className="leftBox">
                                    <h4>
                                        <a href="#">{props.details[3][0].post_title}</a>
                                    </h4>
                                   <div className='text-white'>
                                   {ReactHtmlParser(newsContent[0])}
                                   </div>
                                    <div className="audio py-3">
                                        <audio controls className="w-75" className="d-block">
                                            <source src={props.details[3][0].audio} type="audio/mpeg"/>
                                        </audio>
                                    </div>
                                    <div className="py-3 border-top lborder lgcolor mb-5">
                                        <div className="row align-items-center">
                                            <div className="col-md-5">
                                                <div className="d-flex align-items-center">
                                                    <h6 className="m-0">
                                                        <i className="fas fa-user me-2"></i>
                                                        <Link href={`/news/author/${props.details[5][0].nickname}`}>
                                                        <a>{props.details[5][0].first_name}</a>
                                                        </Link>
                                                    </h6>
                                                    <span>&#160;&#160;|&#160;&#160;{dateFormat(props.details[3][0].post_date)}</span>
                                                </div>
                                            </div>
                                            <div className="col-md-7">
                                                <div className="socials_links news_de_socials text-end">
                                                    <a href="#" className="fab fa-facebook-f"></a>
                                                    <a href="#" className="fab fa-twitter"></a>
                                                    <a href="#" className="fab fa-telegram-plane"></a>
                                                    <a href="#" className="fab fa-pinterest-p"></a>
                                                    <a href="#" className="fab fa-linkedin-in"></a>
                                                    <a href="#" className="fab fa-youtube"></a>
                                                    <a href="#" className="fab fa-instagram"></a>
                                                    <a href="#" className="fas fa-rss"></a>
                                                    <a href="#" className="fab fa-reddit-alien"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {newsContent.map((item, i) => (
                                        (i != '0') &&
                                        <div key={i} className="text-white">
                                        {ReactHtmlParser(item)}
                                        </div>
                                    ))}
                                    <div className="py-4 pill_btns">
                                        {props.details[6][0].Tags.split(", ").map((item, i) => (
                                            <Link 
                                            href={`/news/${tagSlug[i]}`}
                                            key={i} 
                                            onClick={props.getDet}
                                            dataname={`/news/${tagSlug[i]}`}>
                                            <a>{`#${item}`}</a>
                                            </Link>
                                        ))}
                                    </div>
                                    <div className="add py-4">
                                        <img className="w-75" src="/images/NoPath.png"/>
                                    </div>
                                    <div className="post_author p-4 mt-4 inShadowBox rounded">
                                        <div className="d-flex mb-3">
                                            <Image src={replaceImgUrl(props.details[5][0].guid)}
                                                alt={
                                                    props.details[5][0].post_title
                                                }
                                                layout="responsive"
                                                width={80}
                                                height={80}
                                                webp="true"/>
                                            <div>   
                                                <h5 >
                                                    <Link href={`/news/author/${props.details[5][0].nickname}`}>
                                                    <a >{props.details[5][0].first_name+' '+props.details[5][0].lastname}</a>
                                                    </Link>
                                                </h5>
                                            </div>
                                        </div>
                                        <p>{props.details[5][0].description}</p>
                                    </div>

                                </div>
                            </div>
                            <div className="col-md-5">
                                <div className="rightBox">
                                    <div className="post_img mb-4">
                                    <Image src={`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/crypto/wp-content/uploads/${props.details[3][0].image}`
                                                }
                                                alt={
                                                    props.details[3][0].post_title
                                                }
                                                layout="responsive"
                                                width={600}
                                                height={360}
                                                className="rounded w-100 cat_img"
                                                webp="true"/>
                                        </div>
                                    <div className="add mb-5">
                                    <Image src="/images/NoPath.png"
                                                layout="responsive"
                                                width={600}
                                                height={75}
                                                className="w-100"
                                                webp="true"/>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        </>
    )
}