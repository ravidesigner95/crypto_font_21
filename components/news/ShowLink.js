import Link from 'next/link';
export default function ShowLink(props) {
    return (
        <>
            <div className="section mb-5">
                <div className="row">
                    <div className="col-md-5">
                        <Link href={
                            props.url
                        }>
                            <a className="btn outerShadowBtn rounded px-4">
                                Show All</a>
                        </Link>

                    </div>
                    <div className="col-md-7">
                        {/* <div className="">
                            <a href="#" className="d-block">
                                <img className="w-100"
                                    src={
                                        props.adImage
                                    }/>
                            </a>
                        </div> */}
                    </div>
                </div>
            </div>
        </>
    )
}