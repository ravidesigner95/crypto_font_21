export default function Heading(props) {
    return (
        <> {
            (() => {
                if (props.type == 1) {
                    return (
                        <div className="mb-5 text-white">
                            <h4 className="mb-4">{props.heading.title}</h4>
                            <p>{props.heading.description}</p>
                        </div>
                    )
                }
            })()
        } </>
    )
}