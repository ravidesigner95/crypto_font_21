import Link from 'next/link';
import {useState, useEffect} from "react";
import Image from 'next/image';
import ReactHtmlParser from 'react-html-parser';
import {replaceHtml, replaceImgUrl, newsUrl, dateFormat} from '../../helpers/GeneralHelpers';
import SmNews from '../../components/news/smNews';
import FeatureCon from '../includes/FeatureCon';
export default function NewsCategory(props) {

    useEffect(() => {
        // console.log(props.category[3]);
        // console.log(props.getDet);
        if(Array.isArray(props.category[3])){
            console.log("working");
        }else{
            console.log("not working");
        }
        // console.log(Array.isArray(props.category[3]));
    })
    return (
        <>
            <main className="all_news">
                <section className="pt-2 pb-5">
                    <div className="container-fluid px-5">
                        {(() => {
                            if(Array.isArray(props.category[3]) && props.category[3].length){
                                return(
                                <>
                                <FeatureCon image="/images/slider1.jpg" title={props.category[3][0].category_name}/>
                                 </>
                                )
                            }else{
                                return(
                                    <>
                                      <FeatureCon image="/images/slider1.jpg" title="Tag"/>
                                    </>
                                )
                            }
                        })()}
                        <div className="row">
                            {(() => {
                                if(props.category[3].length){
                                    return(
                                        <>
                                          <div className="col-md-5">
                                            <div className="leftBox">
                                                <div className="post_img mb-4">
                                                    <img className="rounded w-100 cat_img" src="/images/slider1.jpg"/></div>
                                                <div className="add mb-5">
                                                    <img className="w-100" src="/images/NoPath.png"/></div>
                                            </div>
                                        </div>
                                         <div className="col-md-7">
                                            <div className="rightBox">
                                                <h4 className="mb-3">
                                                    <a href="#">About&nbsp;&nbsp; 
                                                     {(props.category[3].length)?props.category[3][0].category_name:"Tag"}
                                                    </a>
                                                </h4>
                                                <div className="text-white">
                                                    {/* {ReactHtmlParser(props.category[3][0].description)} */}
                                                    {(props.category[3].length)?replaceHtml(props.category[3][0].description):""}
                                                    </div>
                                            </div>
                                        </div>
                                        </>
                                    )
                                }
                            })()}
                        </div>
                        <div className="row align-items-center">
                            <div className="col-md-7 mb-4">
                                <h4 className="wcolor">Learn Form Our Experts</h4>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                            </div>
                            <div className="col-md-5 mb-4">
                                <div>
                                    <a href="#">
                                        <img className="w-100" src="/images/Mask_Group18.png"/></a>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            {props.category[4].map((item, i) => (
                                  <SmNews type='5' news={item} key={i} getDet={props.getDet}/>
                            ))}
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div>
                                    <nav aria-label="Page navigation example">
                                        <ul className="pagination">
                                            <li className="page-item">
                                                <a className="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                            <li className="page-item active">
                                                <a className="page-link" href="#">1</a>
                                            </li>
                                            <li className="page-item">
                                                <a className="page-link" href="#">2</a>
                                            </li>
                                            <li className="page-item">
                                                <a className="page-link" href="#">3</a>
                                            </li>
                                            <li className="page-item">
                                                <a className="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="text-end">
                                    <a href="#">
                                        <img className="w-75" src="/images/Mask_Group18.png"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </>
    )
}