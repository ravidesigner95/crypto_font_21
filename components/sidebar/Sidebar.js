import React from "react";
import {Tabs, Tab} from "react-bootstrap"
import Link from 'next/link';
import Image from 'next/image';
import {replaceImgUrl, newsUrl, shortText, dateFormat, makeSlug} from '../../helpers/GeneralHelpers';


export default function Sidebar(props) {

    const [currency_key, setCurrencyKey] = React.useState('crypto');
    const [press_key, setpressKey] = React.useState('nft_news');

    return (
            <>
            {props.type == 'currency' ? 
                (<div className="box mb-5 py-4 curreny_status inShadowBox outShadowBox_after rounded">
                    {/* <h5 className="text-center box_head text-white pb-4">Currency</h5> */}
                    {/* <div className="tabs">
                        <a href="#"className="active">Crypto</a>
                        <a href="#">NFT</a>
                        <a href="#">DEFI</a>
                    </div>
                    <ul className="p-0 m-0">
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-success"><i className="fas fa-arrow-up me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                    </ul> */}
                    <Tabs
                        id="currency-tab"
                        activeKey={currency_key}
                        onSelect={(k) => setCurrencyKey(k)}
                        className="border-bottom-0"
                        >
                        <Tab eventKey="crypto" title="Crypto">
                            <ul className="p-0 m-0">
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-success"><i className="fas fa-arrow-up me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                    </ul>
                        </Tab>
                        <Tab eventKey="nft" title="NFT">
                            <ul className="p-0 m-0">
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-success"><i className="fas fa-arrow-up me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                    </ul>
                        </Tab>
                        <Tab eventKey="defi" title="DEFI">
                            <ul className="p-0 m-0">
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-success"><i className="fas fa-arrow-up me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>BTC</span>
                            </div>
                            <div>
                                <strong>$ 48,312.87</strong>
                            </div>
                            <div className="text-end">
                                <span className="text-danger"><i className="fas fa-arrow-down me-2"></i>13.18%</span>
                            </div>
                        </li>
                    </ul>
                        </Tab>
                    </Tabs>
                </div>)

            : props.type == 'pressRelease' ? 

                (<div className="box mb-5 py-4 px-3 sideNews pressReleas inShadowBox outShadowBox_after rounded">
                    <div>
                        <Tabs
                            id="multi_news-tabs"
                            activeKey={press_key}
                            onSelect={(k) => setpressKey(k)}
                            className="border-bottom-0 mb-3"
                            >
                            <Tab eventKey="nft_news" title="NFT News">
                            <ul className="p-0 m-0">
                                {
                                    props.data.map((item, i) => (
                                        <li key={i}>
                                            <div>
                                                <Image src={
                                                        replaceImgUrl(item.image)
                                                    }
                                                    alt={
                                                        item.post_title
                                                    }
                                                    width={80}
                                                    height={80}
                                                    webp="true"
                                                    className="rounded-2"
                                                    />
                                            </div>
                                            <div>
                                                <Link href={
                                                    newsUrl(item.slug)
                                                }>
                                                    <a className="mb-2 d-block">
                                                        {
                                                        shortText(item.post_title, '40')
                                                    }</a>
                                                </Link>
                                                <div className="">
                                                    <small className="text-uppercase lgcolor">
                                                        {
                                                        dateFormat(item.post_date, 1)
                                                    }</small>
                                                </div>
                                            </div>
                                        </li>
                                    ))
                                } </ul>
                                 <div className="text-center py-3">
                                    <Link href="/news/nft">
                                        <a className="btn outerShadowBtn rounded px-4">Show All</a>
                                    </Link>
                                </div>
                            </Tab>
                            <Tab eventKey="defi" title="DEFI News">
                            <ul className="p-0 m-0">
                                {
                                    props.data1.map((item, i) => (
                                        <li key={i}>
                                            <div>
                                                <Image src={
                                                        replaceImgUrl(item.image)
                                                    }
                                                    alt={
                                                        item.post_title
                                                    }
                                                    width={80}
                                                    height={80}
                                                    webp="true"
                                                    className="rounded-2"
                                                    />
                                            </div>
                                            <div>
                                                <Link href={
                                                    newsUrl(item.slug)
                                                }>
                                                    <a className="mb-2 d-block">
                                                        {
                                                        shortText(item.post_title, '40')
                                                    }</a>
                                                </Link>
                                                <div className="">
                                                    <small className="text-uppercase lgcolor">
                                                        {
                                                        dateFormat(item.post_date, 1)
                                                    }</small>
                                                </div>
                                            </div>
                                        </li>
                                    ))
                                } </ul>
                                 <div className="text-center py-3">
                                    <Link href="/news/defi">
                                        <a className="btn outerShadowBtn rounded px-4">Show All</a>
                                    </Link>
                                </div>
                            </Tab>
                        </Tabs>
                    </div>
               </div>)

            : props.type == 'airdrops' ?
               (<div className="box mb-5 py-4 px-3 sideNews airdrop inShadowBox outShadowBox_after rounded">
                    <h5 className="text-center box_head text-white pb-4">Airdrpos</h5>
                    <ul className="p-0 mb-0">
                        {props.data.map((item, i) => (
                             <li key={i}>
                               <Image src={`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/airdrops/${item.logo}`}
                                        alt={
                                            item.post_title
                                        }
                                        width={150}
                                        height={150}
                                        webp="true"
                                        className="rounded-2"
                                        />
                             <div className="m-0 p-3 text-center text">
                                 <Link href={`/airdrop-${makeSlug(item.name+'-'+item.symbol)}`}>
                                 <a >{item.name}</a>
                                 </Link>
                             </div>
                         </li>
                        ))}
                    </ul>
                    <div className="text-center py-3">
                        <Link href={
                            props.sidebar.showLink
                        }>
                            <a href="#" className="px-4 bcolor text-decoration-none">
                                {
                                props.sidebar.showText
                            }
                                <i className="fas fa-arrow-right ms-2"></i>
                            </a>
                        </Link>

                    </div>
                </div>)

            : props.type == 'cryptoGuide' ?

                (<div className="box mb-5 py-4 px-3 sideNews cryptoGuide inShadowBox outShadowBox_after rounded">
                        <h5 className="text-center box_head text-white pb-4">Crypto Guide</h5>
                        <ul className="p-0 mb-0">
                            {props.data.map((item, i) => (
                                    <li key={i}>
                                    <div className="d-flex flex-column">
                                        <small className="bcolor">{dateFormat(item.post_date, 1)}</small>
                                        <Link href={`/news/${item.slug}`}>
                                        <a>{item.post_title}</a>
                                        </Link>
                                    </div>
                                </li>
                            ))}
                        </ul>
                </div>)

            : props.type == 'ads' ?

                (<div className="box mb-5 py-4 px-3 sideNews inShadowBox outShadowBox_after rounded">
                        <h5 className="text-center box_head text-white pb-4">Sponsored Ad</h5>
                        <div className="box mt-4">
                            <img className="w-100"
                                style={
                                    {height: '250px'}
                                }
                                src="/images/"/>
                        </div>
                </div>)

            : props.type == 'eventCalender' ?
                (<div className="box mb-5 py-4 px-3 sideNews eventCal inShadowBox outShadowBox_after rounded">
                    <h5 className="text-center box_head text-white pb-4">Event' Calender</h5>
                    <ul className="p-0 m-0">
                    {props.data.map((item, i) => (
                            <li key={i}>
                            <div>
                            <Image src={
                                        replaceImgUrl(`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/event_images/${item.event_image}`)
                                        }
                                        alt={
                                            item.post_title
                                        }
                                        width={80}
                                        height={80}
                                        webp="true"
                                        className="rounded-2"
                                        />
                            </div>
                            <div>
                                <Link href="#">
                                    <a className="mb-2 d-block">
                                        {item.event_name}
                                    </a>
                                </Link>
                            
                                <div className="d-flex flex-column">
                                    <span>Start : {dateFormat(item.event_start_date,1)}</span>
                                    <span>End : {dateFormat(item.event_end_date, 1)}</span>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
                </div>)

            : null
            
                }
        
             </>           
            )
        }