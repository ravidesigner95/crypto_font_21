import React, {useState, useEffect} from "react";
import Link from "next/link";

function convertMil(value) { // Nine Zeroes for Billions
    return Math.abs(Number(value)) >= 1.0e9 ? (Math.abs(Number(value)) / 1.0e9).toFixed(2) + "B" : // Six Zeroes for Millions
    Math.abs(Number(value)) >= 1.0e6 ? (Math.abs(Number(value)) / 1.0e6).toFixed(2) + "M" : // Three Zeroes for Thousands
    Math.abs(Number(value)) >= 1.0e3 ? (Math.abs(Number(value)) / 1.0e3).toFixed(2) + "K" : Math.abs(Number(value));
}

export default function Header(props) {
    useEffect(() => { // console.log(props.token[0]);
    });

    const nav = [
        {
            name: 'home',
            url: '',
            child: [
                {
                    name:'home',
                    url:''
                },
                {
                    name:'about us',
                    url:'about-us'
                },{
                    name:'our team',
                    url:'our-team'
                },{
                    name:'our partners',
                    url:'partners'
                }
            ]
        },
        {
            name: 'News',
            url: 'news',
            child: []

        },
        {
            name: 'markets',
            url: 'markets',
            child: []
        },
        {
            name: 'cryptopedia',
            url: 'news/cryptopedia',
            child: []
        }, {
            name: 'industry',
            url: '#',
            child: [
                {
                    name: 'Airdrop',
                    url: 'airdrop'
                }, {
                    name: 'Events',
                    url: 'events'
                }, {
                    name: 'stores',
                    url: 'stores'
                }, {
                    name: 'bounties',
                    url: 'bounties'
                }
            ]
        }, {
            name: 'games',
            url: 'games',
            child: []
        }, {
            name: 'NFT',
            url: 'news/nft',
            child: []
        }, {
            name: 'DEFI',
            url: 'news/defi',
            child: []
        }
    ];
    const [navData, setNavData] = useState(nav);
    const [tokenvalue, setTokenValue] = useState({
        value: props.token[0].token_amount,
        percent: (props.token[0].token_amount * 100) / 2500000000
    });
    return (
        <>
            <header>
                <div className="topBar ldbgcolor text-white">
                    <div className="container-fluid px-5 ">
                        <div className="position-relative list_row">
                            <span className="text-white text-uppercase px-4 position-absolute label">
                                Breaking News
                            </span>
                            <marquee>
                                <ul className="m-0 p-0 list-unstyled d-flex text-white">
                                    {
                                    props.news.map((item, i) => (
                                        <li key={i}>
                                            <Link href={
                                                `/news/${
                                                    item.post_name
                                                }`
                                            }>
                                                <a>{
                                                    item.post_title
                                                }</a>
                                            </Link>
                                        </li>
                                    ))
                                } </ul>
                            </marquee>
                        </div>
                    </div>
                </div>
                <div className="container-fluid px-5">
                    <div className="pt-4">
                        <div className="row align-items-center">
                            <div className="col-md-2">
                                {/* <div className="box rounded price_range inShadowBox">
                  <div className="progress gbgcolor" style={{ height: "8px" }}>
                    <div
                      className="progress-bar bbgcolor"
                      role="progressbar"
                      style={{ width: tokenvalue.percent + "%" }}
                      aria-valuenow={tokenvalue.percent}
                      aria-valuemin="0"
                      aria-valuemax="100"
                    ></div>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mt-2">
                    <span className="d-flex flex-column">
                      <strong className="text-white">
                        <small>{convertMil(tokenvalue.value)}</small>
                      </strong>
                      <small className="text-white-50">Tokens Sale</small>
                    </span>
                    <span className="d-flex flex-column align-items-end">
                      <strong className="text-white">
                        <small>2.5 B</small>
                      </strong>
                      <small className="text-white-50">Hard Cap</small>
                    </span>
                  </div>
                </div> */}
                            <Link href="/">
                            <a className="navbar-brand logo me-4">
                                    <img src="/images/logo.png"/>
                                </a>
                            </Link>
                                
                            </div>
                            <div className="col-md-5 offset-md-1">
                                <div className="box">
                                    <img className="w-100" src="/images/top_add.png"/>
                                </div>
                            </div>
                            <div className="col-md-3 offset-md-1">
                                <div className="box search position-relative">
                                    <i className="fas fa-search position-absolute"></i>
                                    <input type="text" className="form-control ps-5 bg-transparent border-0 rounded inShadowBox" placeholder="Search here"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="py-4">
                        <div className="menu">
                            <nav className="navbar navbar-expand-lg navbar-dark bg-transparent py-3 px-4 outShadowBox rounded">
                                <div className="container-fluid">
                                    {/* <a className="navbar-brand logo me-4" href="#">
                    <img src="images/logo.png" />
                  </a> */}
                                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"></span>
                                    </button>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        {/* ${(Router.router.route == '/'+item.url)?'active':""} */}
                                        <ul className="navbar-nav me-auto mt-2">
                                            {
                                            nav.map((item, i) => ((item.child.length) ? <li className="nav-item dropdown"
                                                key={i}>
                                                <a className={`nav-link dropdown-toggle`}
                                                    href="#"
                                                    id="navbarDropdown"
                                                    data-bs-toggle="dropdown">
                                                    {
                                                    item.name
                                                } </a>

                                                <ul className="dropdown-menu">
                                                    {
                                                    item.child.map((item2, j) => (
                                                        <li key={j}>
                                                            <Link href={
                                                                `/${
                                                                    item2.url
                                                                }`
                                                            }>
                                                                <a className="dropdown-item">
                                                                    {
                                                                    item2.name
                                                                }</a>
                                                            </Link>
                                                        </li>
                                                    ))
                                                } </ul>
                                            </li> : <li className="nav-item"
                                                key={i}>
                                                <Link href={
                                                    `/${
                                                        item.url
                                                    }`
                                                }>
                                                    <a className={`nav-link`}>
                                                        {
                                                        item.name
                                                    }</a>
                                                </Link>
                                            </li>))
                                        }; {/* <li className="nav-item">
                                                <a className="nav-link" href="#">News Feed</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#">
                                                    CryptoWorld
                                                </a>
                                                <ul className="dropdown-menu">
                                                    <li>
                                                        <a className="dropdown-item" href="#"><img className="me-3" src="images/menu_icons/game-development.png"/><span>Action</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="dropdown-item" href="#"><img className="me-3" src="images/menu_icons/game-development.png"/><span>Another action</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#">
                                                    Blockchain
                                                </a>
                                                <ul className="dropdown-menu">
                                                    <li>
                                                        <a className="dropdown-item" href="#"><img className="me-3" src="images/menu_icons/game-development.png"/><span>Action</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="dropdown-item" href="#"><img className="me-3" src="images/menu_icons/game-development.png"/><span>Another action</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#">
                                                    ICO's
                                                </a>
                                                <ul className="dropdown-menu">
                                                    <li>
                                                        <a className="dropdown-item" href="#">Action</a>
                                                    </li>
                                                    <li>
                                                        <a className="dropdown-item" href="#">Another action</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#">
                                                    Events
                                                </a>
                                                <ul className="dropdown-menu">
                                                    <li>
                                                        <a className="dropdown-item" href="#">Action</a>
                                                    </li>
                                                    <li>
                                                        <a className="dropdown-item" href="#">Another action</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#">
                                                    Analysis
                                                </a>
                                                <ul className="dropdown-menu">
                                                    <li>
                                                        <a className="dropdown-item" href="#">Action</a>
                                                    </li>
                                                    <li>
                                                        <a className="dropdown-item" href="#">Another action</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Prediction</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Dapps</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link login_btn" href="#">Login</a>
                                            </li> */} </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </>
    );
}