import Image from 'next/image';
import Link from 'next/link'; 
export default function RelatedCon(params) {
    return (
        <>
            <div className="row align-items-center">
                <div className="col-md-7 mb-5">
                    <h4 className="wcolor">Popular Events</h4>
                    <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text.
                        It has roots in a piece of classical Latin literature from 45 BC,
                        making it over 2000 years old. It uses a dictionary of over 200
                        Latin words, combined with a handful of model sentence structures,
                        to generate Lorem Ipsum which looks reasonable.
                    </p>
                </div>
                <div className="col-md-5 mb-5">
                    <div>
                        <a href="#">
                            <img className="w-100" src="/images/Mask_Group18.png"/>
                        </a>
                    </div>
                </div>
            </div>
            <div className="p-4 outShadowBox rounded mb-5">
                <div className="row">
                    {
                    [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6
                    ].map((i) => (
                        <div key={i}
                            className="col-md-6 mb-4">
                            <div className="box inShadowBox rounded p-3">
                                {
                                i < 3 ? <span className={
                                    `sticker text-uppercase position-absolute ${
                                        i == 1 ? 'orange' : 'blue'
                                    }`
                                }>Featured</span> : null
                            }
                                <div className="row">
                                    <div className="col-md-3 pe-0">
                                        <Link href="#">
                                            <a className="d-block">
                                                <Image src="/images/img1.jpg"
                                                    height={150}
                                                    width={150}
                                                    webp="true"
                                                    className="rounded2 obj_fit_cover"/>
                                            </a>
                                        </Link>
                                    </div>
                                    <div className="col-md-9 ps-4 ps-md-0">
                                        <h6>
                                            <Link href="#">
                                                <a>Blockchain Expo Global 2020</a>
                                            </Link>
                                        </h6>
                                        <div className="d-flex flex-column mb-3 lgcolor">
                                            <span>Token : Digimon</span>
                                            <span>Start : 17th Mar 2021</span>
                                            <span>End : 18th Mar 2021</span>
                                        </div>
                                        <div className="circle_small_social">
                                            <a href="#" className="fab fa-facebook-f"></a>
                                            <a href="#" className="fab fa-twitter"></a>
                                            <a href="#" className="fab fa-telegram-plane"></a>
                                            <a href="#" className="fab fa-pinterest-p"></a>
                                            <a href="#" className="fab fa-linkedin-in"></a>
                                            <a href="#" className="fab fa-youtube"></a>
                                            <a href="#" className="fab fa-instagram"></a>
                                            <a href="#" className="fas fa-rss"></a>
                                            <a href="#" className="fab fa-reddit-alien"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                } </div>
            </div>
        </>
    )
};