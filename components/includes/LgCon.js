import Image from 'next/image';
import Link from 'next/link';
import {dateFormat, replaceHtml} from '../../helpers/GeneralHelpers';
export default function LgCon(props) {
    return (<>
        <div className="p-4 py-5 rounded inShadowBox mb-4">
            <div className="row align-items-center">
                <div className="col-md-6">
                    <div className="row">
                        <div className="col-md-3">
                            <div>
                                <Image src={
                                        props.item.image
                                    }
                                    alt={
                                        props.item.title
                                    }
                                    height={150}
                                    width={150}
                                    webp="ture"
                                    className="rounded2 obj_fit_cover"/>
                            </div>
                        </div>
                        <div className="col-md-9">
                            <div>
                                <h5 className="dwcolor">
                                    {
                                    props.item.title
                                }</h5>
                                <div className="d-flex flex-column lgcolor">
                                    {
                                    (() => {
                                        if (props.item.type == "event") {
                                            return (
                                                <>
                                                    <span className="wcolor mb-2">
                                                        <strong className="bcolor">Location :</strong>
                                                        {
                                                        props.item.data.location
                                                    } </span>
                                                    <span>Start : {
                                                        dateFormat(props.item.data.start)
                                                    }</span>
                                                    <span>End : {
                                                        dateFormat(props.item.data.end)
                                                    }</span>
                                                </>
                                            )
                                        }
                                    })()
                                }

                                    {
                                    (() => {
                                        if (props.item.type == "airdrop") {
                                            return (
                                                <>
                                                    <span className="wcolor mb-2">
                                                        <strong className="bcolor">Symbol :</strong>
                                                        {
                                                        props.item.data.token
                                                    } </span>
                                                    <span className="wcolor mb-2">
                                                        <strong className="bcolor">value :</strong>
                                                        {
                                                        props.item.data.value
                                                    } </span>
                                                    <span>Start : {
                                                        dateFormat(props.item.data.start)
                                                    }
                                                        - End : {
                                                        dateFormat(props.item.data.end)
                                                    }</span>
                                                </>
                                            )
                                        }
                                    })()
                                } </div>
                            </div>
                        </div>
        </div>
    </div>
                <div className="col-md-6">
                    <div className="text-end mb-5">
                    {
                                    (() => {
                                        if (props.item.type == "event" || props.item.type == "dapps") {
                                            return (
                                                <>
                                                    <a href={
                                (props.item.data.website != "") ? props.item.data.website : "#"
                            }
                            target="_blank"
                            className="crclBtn d-inline-block">
                            Visit Website
                        </a>
                                                </>
                                            )
                                        }
                                    })()
                                }
                    </div>
                    <div className="circle_small_social text-end">
                        <a href="#" className="fab fa-facebook-f"></a>
                        <a href="#" className="fab fa-twitter"></a>
                        <a href="#" className="fab fa-telegram-plane"></a>
                        <a href="#" className="fab fa-pinterest-p"></a>
                        <a href="#" className="fab fa-linkedin-in"></a>
                        <a href="#" className="fab fa-youtube"></a>
                        <a href="#" className="fab fa-instagram"></a>
                        <a href="#" className="fas fa-rss"></a>
                        <a href="#" className="fab fa-reddit-alien"></a>
                    </div>
                </div>
            </div>
            <div className="row">
            <div className="col-md-12">
                            <p>{
                                replaceHtml(props.item.data.description)
                            }</p>
                            {
                            (() => {
                            if (props.item.type == "airdrop") {
                                return (
                                    <>
                                        <p>Step by step :</p>
                                        <p>Click here to Join : <a href={props.item.data.website}>Digimon Swap  Airdrop</a></p>
                                        <p>Join Telegram: </p>
                                        <p>Follow Twitter</p>
                                        <p>Submit BSC Wallet</p>
                                        <p>Don't forget to join our <a href="https://t.me/cryptoknowmic">Telegram channel</a>, follow us <a href="https://www.facebook.com/cryptoknowmics/">Facebook</a>, <a href="https://www.linkedin.com/company/14484695/admin/">Linkedin</a>, <a href="https://in.pinterest.com/cryptoknowmics/">Pinterest</a>, and <a href="https://twitter.com/official_ckm">Twitter</a> to receive new airdrops!</p>
                                        <div className="text-center">
                                            <a href={props.item.data.website}>claim Digimon Swap  airdrop</a>
                                        </div>
                                    </>
                                )
                            }
                        })()
                    }
            </div>
            </div>
        </div>
    </>
        )
    };