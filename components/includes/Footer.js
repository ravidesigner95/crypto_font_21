import Link from 'next/link';
export default function Footer() {
    return (
        <>
            <footer className="pb-5">
                <div className="">
                    <div className="container-fluid px-5">
                        <div className="box p-4 outShadowBox rounded single_post">
                            <div className="border-bottom lborder">
                                <div className="row align-items-center">
                                    <div className="col-md-2 mb-4">
                                        <Link href="/">
                                            <a className="logo">
                                                <img src="/images/logo.png"/></a>
                                        </Link>
                                    </div>
                                    <div className="col-md-5 mb-4">
                                        <div className="nav">
                                            <Link href="/terms-of-use">
                                                <a>Terms of use</a>
                                            </Link>
                                            <Link href="/privacy-policy">
                                                <a>Privacy Policy</a>
                                            </Link>
                                            <Link href="/disclaimer">
                                                <a>Disclaimer</a>
                                            </Link>
                                            <Link href="/sitemap.html">
                                                <a>Sitemap</a>
                                            </Link>

                                        </div>
                                    </div>
                                    <div className="col-md-5 mb-4">
                                        <div className="foot_socials">
                                            <Link href="https://www.facebook.com/cryptoknowmics/">
                                                <a className="fab fa-facebook-f"></a>
                                            </Link>
                                            <Link href="https://twitter.com/official_ckm">
                                                <a className="fab fa-twitter"></a>
                                            </Link>
                                            <Link href="https://telegram.me/cryptoknowmic">
                                                <a className="fab fa-telegram-plane"></a>
                                            </Link>
                                            <Link href="https://www.pinterest.co.uk/cryptoknowmics">
                                                <a className="fab fa-pinterest-p"></a>
                                            </Link>
                                            <Link href="https://www.linkedin.com/company/cryptoknowmics/">
                                                <a className="fab fa-linkedin-in"></a>
                                            </Link>
                                            <Link href=" https://www.youtube.com/c/Cryptoknowmics">
                                                <a className="fab fa-youtube"></a>
                                            </Link>
                                            <Link href=" https://www.instagram.com/cryptoknowmic/">
                                                <a className="fab fa-instagram"></a>
                                            </Link>
                                            <Link href="https://www.cryptoknowmics.com/news/feed">
                                                <a className="fas fa-rss"></a>
                                            </Link>
                                            <Link href="https://www.reddit.com/r/ckm_official/">
                                                <a className="fab fa-reddit-alien"></a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="py-4">
                                <div className="row">
                                    <div className="col-md-2 mb-4 mb-md-0">
                                        <div className="box">
                                            <h5 className="mb-4 text-white">Features</h5>
                                            <ul className="m-0 p-0 list-unstyled">
                                                <li>
                                                    <Link href="/events">
                                                        <a>Events</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/icos">
                                                        <a>ICOs</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/ieos">
                                                        <a>IEOs</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/stos">
                                                        <a>STOs</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/dapps">
                                                        <a>Dapps</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/airdrop">
                                                        <a>Airdrop</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/bounty">
                                                        <a>Bounty</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                    <Link href="/arbitrage-exchange">
                                                        <a>Arbitrage</a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-2 mb-4 mb-md-0">
                                        <div className="box">
                                            <h5 className="mb-4 text-white">News</h5>
                                            <ul className="m-0 p-0 list-unstyled">
                                                <li>
                                                    <Link href="/cryptocurrency-news">
                                                    <a>Live Crypto News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/bitcoin">
                                                    <a>Live Bitcoin News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/ethereum">
                                                    <a>Live Ethereum News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/altcoins">
                                                    <a>Live Altcoin News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/crypto-tutorial">
                                                    <a>Latest Blockchain News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/regulation">
                                                    <a>Latest Regulation News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/litecoin">
                                                    <a>Live Litecoin News</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/ripple">
                                                    <a>Live Ripple News</a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-2 mb-4 mb-md-0">
                                        <div className="box">
                                            <h5 className="mb-4 text-white">Industry</h5>
                                            <ul className="m-0 p-0 list-unstyled">
                                                <li>
                                                <Link href="/news-feed">
                                                    <a>News Feeds</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="https://blog.cryptoknowmics.com/">
                                                    <a>Blogs</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/newsletters">
                                                    <a>Newsletters</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/videos">
                                                    <a>Videos</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/podcast">
                                                    <a>Prodcast</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="https://www.forum.cryptoknowmics.com/">
                                                    <a>Forum</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/crypto-jobs">
                                                    <a>Jobs</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="https://store.cryptoknowmics.com/">
                                                    <a>Store</a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-2 mb-4 mb-md-0">
                                        <div className="box">
                                            <h5 className="mb-4 text-white">Market Analysis</h5>
                                            <ul className="m-0 p-0 list-unstyled">
                                                <li>
                                                <Link href="/gainers">
                                                    <a>Biggest Crypto Gainers</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/losers">
                                                    <a>Biggest Crypto Losers</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/news/technical-analysis">
                                                    <a>Crypto Technical Analysis</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/white-papers">
                                                    <a>Crypto White Paper</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/news/crypto-review-report">
                                                    <a>Cryptocurrency Reports</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/news/crypto-review-report">
                                                    <a>Cryptocurrency Reviews</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/wallets">
                                                    <a>Cryptocyrrency Wallets</a>
                                                    </Link>
                                                </li>
                                                <li>
                                                <Link href="/whale-alert">
                                                    <a>Whale Alert</a>
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="row">
                                            <div className="col-md-7 mb-5">
                                                <div className="box pe-4">
                                                    <h5 className="mb-4 text-white">Newsletter Sign up</h5>
                                                    <p className="wcolor mb-4">Subscribe now and get exclusive news, interviews and stories</p>
                                                    <div className="mb-4">
                                                        <input type="text" className="form-control" placeholder="Enter Email Address"/>
                                                    </div>
                                                    <div className="mb-3">
                                                        <button type="submit" className="btn rounded-pill py-2 px-4 wcolor bbgcolor">Subscribe</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-5">
                                                <div className="box footAdds">
                                                    <a href="#" className="d-block">
                                                        <img className="w-100" src="/images"
                                                            style={
                                                                {height: '200px'}
                                                            }/></a>
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="foo_add">
                                                    <a href="#">
                                                        <img className="w-100" src="/images/NoPath.png"/></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="border-top sb_foot lborder text-center pt-3">
                                <p className="lgcolor m-0">Copyright &copy; 2021 Cryptoknowmics & Developed By &nbsp;&nbsp;
                                    <a className="bcolor" href="https://agiosupport.com" targe="_blank">Agio Support Solutions PVT.LTD.</a>
                                    &nbsp;&nbsp;All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}