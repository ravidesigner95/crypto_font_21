import SocialShare from "./SocialShare";
import Link from 'next/link';
import Image from 'next/image';
import {makeSlug, replaceHtml, dateFormat} from '../../helpers/GeneralHelpers';
export default function SmCon(props) {
    return (
        <>
            <div className="col-md-6 mb-4">
                <div className="box event_box inShadowBox rounded p-3">
                    {
                    (() => {
                        if (props.item.premium == 1) {
                            return (
                                <>
                                    <span className="sticker text-uppercase position-absolute">Premium</span>
                                </>
                            )
                        } else if (props.item.featured == 1) {
                            return (
                                <>
                                    <span className="sticker text-uppercase position-absolute">Featured</span>
                                </>
                            )
                        } else if (props.item.latest == 1) {
                            return (
                                <>
                                    <span className="sticker text-uppercase position-absolute">Latest</span>
                                </>
                            )
                        }
                    })()
                }
                    <div className="row">
                        <div className="col-md-3 pe-0">
                            <Link href={
                                makeSlug(props.item.slug)
                            }>
                                <a className="d-block">
                                    <Image src={
                                            props.item.image
                                        }
                                        height={150}
                                        width={150}
                                        webp="true"/>
                                </a>
                            </Link>
                        </div>
                        <div className="col-md-9 ps-4 ps-md-0">
                            <h6>
                                <Link href={
                                    makeSlug(props.item.slug)
                                }>
                                    <a>{
                                        props.item.title
                                    }</a>
                                </Link>
                            </h6>
                            {
                            (() => {
                                if (props.item.type == "event") {
                                    return (
                                        <>
                                            <div className="d-flex flex-column mb-3">
                                                <span>Location : {
                                                    props.item.event.location
                                                }</span>
                                                <span>Start : {
                                                    dateFormat(props.item.event.start)
                                                }</span>
                                                <span>End : {
                                                    dateFormat(props.item.event.end)
                                                }</span>
                                            </div>
                                        </>
                                    )
                                } else if (props.item.type == "airdrop") {
                                    return (
                                        <>
                                            <div className="d-flex flex-column mb-3">
                                                <span>Token : {
                                                    props.item.airdrop.token
                                                }</span>
                                                <span>Start : {
                                                   dateFormat( props.item.airdrop.start)
                                                }</span>
                                                <span>End : {
                                                    dateFormat(props.item.airdrop.end)
                                                }</span>
                                            </div>
                                        </>
                                    )
                                } else if(props.item.type == "dapps"){
                                    return (
                                        <>
                                            <div className="mb-3">
                                                <p>{replaceHtml(props.item.dapps.description)}</p>
                                            </div>
                                        </>
                                    )
                                }
                            })()
                        }
                            <SocialShare/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}