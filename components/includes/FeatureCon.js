import Image from 'next/image';
import Link from 'next/link';
export default function FeatureCon(props) {
    return (
        <>
            <div className="innerBanner p-3 rounded inShadowBox mb-4">
                <div className="overflow-hidden position-relative h-100 rounded">
                    {/* <img className="rounded" src=""/> */}
                    <Image src={props.image}
                        alt={
                            props.title
                        }
                        layout="responsive"
                        width={1455}
                        height={268}
                        className="d-block w-100"
                        webp="true"/>
                    <div className="content text-uppercase">
                        <h2 className="pb-4 text-white">{props.title}</h2>
                        <span className="text-white">
                            <Link href="/">
                                <a className="bcolor">Home</a>
                            </Link>
                            / {props.title}</span>
                    </div>
                </div>
            </div>
        </>
    )
};