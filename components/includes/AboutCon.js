import Image from 'next/image';
import Link from 'next/link';
export default function AboutCon(props) {
    return (
        <>
            <div className="row">
                <div className="col-md-5">
                    <div className="leftBox">
                        <div className="post_img mb-4">
                            <Image src="/images/slider1.jpg" layout="responsive"
                                width={400}
                                height={250}
                                className="rounded"/>
                        </div>
                        <div className="add mb-5">
                            <img className="w-100" src="images/NoPath.png"/>
                        </div>
                    </div>
                </div>
                <div className="col-md-7">
                    <div className="rightBox">
                        <h4 className="mb-3 wcolor">About Cryptopedia</h4>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a
                            <a href="#" class="bcolor">passage of Lorem Ipsum</a>, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a you need to be sure there isn't anything embarrassing hidden in the middle of text to
                            <a href="#" class="bcolor">passage of Lorem Ipsum</a>, generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                    </div>
                </div>
            </div>
            <div className="row align-items-center">
                <div className="col-md-7 mb-4">
                    <h4 className="wcolor">Learn Form Our Experts</h4>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                </div>
                <div className="col-md-5 mb-4">
                    <div>
                        <a href="#"><img className="w-100" src="images/Mask_Group18.png"/></a>
                    </div>
                </div>
            </div>
        </>
    )
}