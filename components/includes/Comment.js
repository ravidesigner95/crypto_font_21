import Image from 'next/image';
import Link from 'next/link'; 
export default function Comment(params) {
    return (
        <>
            <div className="p-4 outShadowBox rounded mb-5">
                <div className="row">
                    <div className="col-md-6 mb-5">
                        <h5 className="dwcolor">Leave a Comments</h5>
                        <p>
                            Contrary to popular belief, Lorem Ipsum is not simply random
                                                                                                                        text. It has roots in a piece of classical Latin literature from
                                                                                                                        45 BC
                        </p>
                        <form>
                            <div className="mb-4">
                                <textarea className="form-control inShadowBox rounded p-3 bg-transparent border-0 lgcolor" placeholder="add a comment..." rows="4"></textarea>
                            </div>
                            <div>
                                <button className="crclBtn border-0">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-md-6 mb-5">
                        <div className="d-flex aling-items-center justify-content-center my-4">
                            <strong className="bcolor">Share On :</strong>
                            <div className="circle_small_social text-end ms-4">
                                <a href="#" className="fab fa-facebook-f"></a>
                                <a href="#" className="fab fa-twitter"></a>
                                <a href="#" className="fab fa-telegram-plane"></a>
                                <a href="#" className="fab fa-pinterest-p"></a>
                                <a href="#" className="fab fa-linkedin-in"></a>
                                <a href="#" className="fab fa-youtube"></a>
                                <a href="#" className="fab fa-instagram"></a>
                                <a href="#" className="fas fa-rss"></a>
                                <a href="#" className="fab fa-reddit-alien"></a>
                            </div>
                        </div>
                        <div className="mb-4">
                            <h5 className="dwcolor">Enter Shared Links</h5>
                            <p>
                                Contrary to popular belief, Lorem Ipsum is not simply random
                                                                                                                                      text.
                            </p>
                            <div className="inShadowBox rounded p-3 d-flex">
                                <input type="text" className="form-control bg-transparent border-0 shadow-none lgcolor" placeholder="Ex@https://facebook.com/@youid/sharedlink"/>
                                <input type="submit" value="Send" className="crclBtn rounded border-0"/>
                            </div>
                        </div>
                        <div className="mb-4 text-end">
                            <div>
                                <a href="#">
                                    <img className="w-75" src="/images/Mask_Group18.png"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="comment">
                    <h4 className="mb-4 dwcolor">Recents Comments</h4>
                    {
                    [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6
                    ].map((i) => (
                        <div key={i}
                            className="commentBox rounded inShadowBox p-4 mb-4">
                            <div className="commentHead mb-3">
                                <div className="d-flex align-items-center">
                                    <Link href="#">
                                        <a>
                                            <Image src="/images/img1.jpg"
                                                width={40}
                                                height={40}
                                                webp="true"
                                                className="rounded-circle"/>
                                        </a>
                                    </Link>
                                    <div className="ms-3 d-flex flex-column">
                                        <Link href="#">
                                            <a>@Nimisha</a>
                                        </Link>
                                        <small className="lgcolor">5 days ago</small>
                                    </div>
                                </div>
                            </div>
                            <div className="body">
                                <p className="m-0">
                                    There are many variations of passages of Lorem Ipsum
                                                                                                                                                    available, but the majority have suffered alteration in some
                                                                                                                                                    form, by injected humour, or randomised words which don't
                                                                                                                                                    look even slightly believable. If you are going to use a
                                                                                                                                                    passage of Lorem Ipsum, you need to be sure there isn't
                                                                                                                                                    anything embarrassing hidden in the middle of text.
                                </p>
                            </div>
                        </div>
                    ))
                } </div>
            </div>
        </>
    )
};