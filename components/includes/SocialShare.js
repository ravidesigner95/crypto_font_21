import Link from 'next/link';
import Image from 'next/image';
export default function SocialShare(props) {
    return (
        <>
            <div class="social_link">
                <a href="#" className="fab fa-facebook-f"></a>
                <a href="#" className="fab fa-twitter"></a>
                <a href="#" className="fab fa-telegram-plane"></a>
                <a href="#" className="fab fa-pinterest-p"></a>
                <a href="#" className="fab fa-linkedin-in"></a>
                <a href="#" className="fab fa-youtube"></a>
                <a href="#" className="fab fa-instagram"></a>
                <a href="#" className="fas fa-rss"></a>
                <a href="#" className="fab fa-reddit-alien"></a>
            </div>
        </>
    )
}