import React from 'react';
import Footer from './includes/Footer';
import Header from './includes/Header';

const Layout = props => {
    const {breakingNews, token, children} = props;
    React.useEffect(()=>{
        // console.log(breakingNews)
    })
    return (<>
         <div>
                <Header news={breakingNews} token={token}/>
                {children}
                <Footer/>
            </div>
    </>)
}
export default Layout;
