<div className="col-md-3">
<div className="rightBox">
    <div className="box mb-5 py-4 curreny_status inShadowBox outShadowBox_after rounded">
        <h5 className="text-center box_head text-white pb-4">Currency</h5>
        <ul className="p-0 m-0">
            <li>
                <div>
                    <span>BTC</span>
                    <strong>01</strong>
                </div>
                <div>
                    <span>=</span>
                </div>
                <div>
                    <span>USD</span>
                    <strong>48,312.87</strong>
                </div>
            </li>
            <li>
                <div>
                    <span>BTC</span>
                    <strong>01</strong>
                </div>
                <div>
                    <span>=</span>
                </div>
                <div>
                    <span>USD</span>
                    <strong>48,312.87</strong>
                </div>
            </li>
            <li>
                <div>
                    <span>BTC</span>
                    <strong>01</strong>
                </div>
                <div>
                    <span>=</span>
                </div>
                <div>
                    <span>USD</span>
                    <strong>48,312.87</strong>
                </div>
            </li>
        </ul>
    </div>
    <div className="box mb-5 py-4 px-3 sideNews eventCal inShadowBox outShadowBox_after rounded">
        <h5 className="text-center box_head text-white pb-4">Event's Calender</h5>
        <ul className="p-0 m-0">
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020</a>
                    <div className="d-flex flex-column">
                        <span>Start : Mar 17,2021</span>
                        <span>End : Mar 18,2021</span>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020</a>
                    <div className="d-flex flex-column">
                        <span>Start : Mar 17,2021</span>
                        <span>End : Mar 18,2021</span>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020</a>
                    <div className="d-flex flex-column">
                        <span>Start : Mar 17,2021</span>
                        <span>End : Mar 18,2021</span>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020</a>
                    <div className="d-flex flex-column">
                        <span>Start : Mar 17,2021</span>
                        <span>End : Mar 18,2021</span>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020</a>
                    <div className="d-flex flex-column">
                        <span>Start : Mar 17,2021</span>
                        <span>End : Mar 18,2021</span>
                    </div>
                </div>
            </li>
        </ul>
        <div className="text-center py-3">
            <a href="#" className="btn outerShadowBtn rounded px-4">View All Events</a>
        </div>
    </div>
    <div className="box mb-5 py-4 px-3 sideNews pressReleas inShadowBox outShadowBox_after rounded">
        <h5 className="text-center box_head text-white pb-4">Press Releases</h5>
        <ul className="p-0 m-0">
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020 expo global</a>
                    <div className="text-end">
                        <small className="text-uppercase lgcolor">Jan 22,2021 10:30 am</small>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020 expo global</a>
                    <div className="text-end">
                        <small className="text-uppercase lgcolor">Jan 22,2021 10:30 am</small>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020 expo global</a>
                    <div className="text-end">
                        <small className="text-uppercase lgcolor">Jan 22,2021 10:30 am</small>
                    </div>
                </div>
            </li>
            <li>
                <div>
                    <img src="/images/"/>
                </div>
                <div>
                    <a href="#" className="mb-2 d-block">Blockchain expo global 2020 expo global</a>
                    <div className="text-end">
                        <small className="text-uppercase lgcolor">Jan 22,2021 10:30 am</small>
                    </div>
                </div>
            </li>
        </ul>
        <div className="text-center py-3">
            <a href="#" className="btn outerShadowBtn rounded px-4">View All Events</a>
        </div>
    </div>
    <div className="box mb-5 py-4 px-3 sideNews cryptoGuid inShadowBox outShadowBox_after rounded">
        <h5 className="text-center box_head text-white pb-4">Crypto Guide</h5>
        <ul className="p-0 mb-0">
            <li>
                <img src="images"/>
                <div className="m-0 p-3 text-center">
                    <a href="#">Some text for image box...</a>
                </div>
            </li>
            <li>
                <img src="images"/>
                <div className="m-0 p-3 text-center">
                    <a href="#">Some text for image box...</a>
                </div>
            </li>
            <li>
                <img src="images"/>
                <div className="m-0 p-3 text-center">
                    <a href="#">Some text for image box...</a>
                </div>
            </li>
            <li>
                <img src="images"/>
                <div className="m-0 p-3 text-center">
                    <a href="#">Some text for image box...</a>
                </div>
            </li>
        </ul>
        <div className="text-center py-3">
            <a href="#" className="px-4 bcolor text-decoration-none">View All Events
                <i className="fas fa-arrow-right ms-2"></i>
            </a>
        </div>
    </div>
    <div className="box mb-5 py-4 px-3 sideNews cryptoNews inShadowBox outShadowBox_after rounded">
        <h5 className="text-center box_head text-white pb-4">Crypto News</h5>
        <ul className="p-0 mb-0">
            <li>
                <div className="d-flex flex-column">
                    <small className="bcolor">12:17PM</small>
                    <a href="#">Contrary to popular belief, Lorem Ipsum is not simply random text.</a>
                </div>
            </li>
            <li>
                <div className="d-flex flex-column">
                    <small className="bcolor">12:17PM</small>
                    <a href="#">Contrary to popular belief, Lorem Ipsum is not simply random text.</a>
                </div>
            </li>
            <li>
                <div className="d-flex flex-column">
                    <small className="bcolor">12:17PM</small>
                    <a href="#">Contrary to popular belief, Lorem Ipsum is not simply random text.</a>
                </div>
            </li>
            <li>
                <div className="d-flex flex-column">
                    <small className="bcolor">12:17PM</small>
                    <a href="#">Contrary to popular belief, Lorem Ipsum is not simply random text.</a>
                </div>
            </li>
            <li>
                <div className="d-flex flex-column">
                    <small className="bcolor">12:17PM</small>
                    <a href="#">Contrary to popular belief, Lorem Ipsum is not simply random text.</a>
                </div>
            </li>
        </ul>
    </div>
    <div className="box mb-5 py-4 px-3 sideNews add_Box inShadowBox outShadowBox_after rounded">
        <h5 className="text-center box_head text-white pb-4">Heading Ads</h5>
        <div className="box mt-4">
            <img className="w-100"
                style={
                    {height: '250px'}
                }
                src="/images/"/>
        </div>
    </div>
</div>
</div>