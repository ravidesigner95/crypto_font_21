import Head from 'next/head'
import {useState, useEffect} from "react";
import React from 'react';
import Image from 'next/image';
import Link from 'next/link'; 
import Layout from '../../components/Layout';
// import LgCon from '../../components/includes/LgCon';
import LgCon from '../../components/includes/LgCon'
import Comment from '../../components/includes/Comment';
import RelatedCon from '../../components/includes/RelatedCon';
// import { replaceHtml } from '../../helpers/GeneralHelpers';
import {replaceHtml} from '../../helpers/GeneralHelpers';

export async function getServerSideProps(context) {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/icos-details?slug=${
        context.params.slug
    }`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}

const icosDetail = ({data})=>{

    return (
        <>
            <Head>
                <title>ICOs Details</title>
            </Head>
            <Layout breakingNews={
                    data[0]
                }
                token={
                    data[1]
            }>
                <main>
                    <div className="container-fluid px-5">
                       <LgCon item={
                           {
                               type:'icos',
                               title:data[2][0].title,
                               featured:'',
                               latest:'',
                               premium:'',
                               image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/icos/${data[2][0].image}`,
                               event:{
                                   location:data[2][0].event_location,
                                   start:data[2][0].start_date,
                                   end:data[2][0].end_date,
                                   website:data[2][0].website_link,
                                   whitepaper:data[2][0].whitepaper_link,
                                   ticket:data[2][0].event_ticket,
                                   description:data[2][0].description}
                                   }
                                   }/>
                        <div className="py-4">
                            <p>{replaceHtml(data[2][0].description)}</p>
                        </div>
                      <Comment/>
                      <RelatedCon/>
                    </div>
                </main>

            </Layout>
        </>
    )
}

export default icosDetail;