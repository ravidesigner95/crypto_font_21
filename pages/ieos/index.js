import React from 'react'
import Image from 'next/image';
import Link from 'next/link';
import FeatureCon from '../../components/includes/FeatureCon';
import Head from 'next/head';
import Layout from '../../components/Layout';
import IcosCom from '../../components/IcosCom';
import AboutCon from '../../components/includes/AboutCon';
import { makeSlug } from '../../helpers/GeneralHelpers';

export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/ongoing-ieos`)
    const data = await res.json()
    // console.log(data[0]);
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}

const index = ({data}) => {
    const [ieosdata, setieosdata] = React.useState(data);
    return (
        <>
            <Head>
                <title>IEOS | Cryptoknowmics</title>
                <link rel="icon" href="https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/logo/favicon.ico"/>
            </Head>
            <Layout breakingNews={
                    ieosdata[0]
                }
                token={
                    ieosdata[1]
            }>
                <main>
                    <div className="container-fluid px-5">
                        <FeatureCon image="/images/slider1.jpg" title="IEOS List"/>
                        <AboutCon/>
                        <div className="row">
                            <div className="col-md-9">
                                <div className="p-4 outShadowBox rounded mb-5">
                                    <div className="tabs mb-4">
                                        <a href="#" className="active">All ICOs</a>
                                        <a href="#">Onging ICOs</a>
                                        <a href="#">Upcoming ICOs</a>
                                        <a href="#">Past ICOs</a>
                                    </div>
                                    <div>
                                        <div className="row dwcolor px-4 mb-3">
                                            <div className="col-md-8">
                                                <strong>Name</strong>
                                            </div>
                                            <div className="col-md-2">
                                                <strong>Start Date</strong>
                                            </div>
                                            <div className="col-md-2">
                                                <strong>End Date</strong>
                                            </div>
                                        </div>

                                        <div className="inShadowBox rounded p-4">
                                            {ieosdata[2].map((item,i)=>(
                                                <IcosCom key={i}
                                                    pro_data={{
                                                    arr_length:ieosdata[2].length,
                                                    id:i,
                                                    type:'ieos',
                                                    title:item.title,
                                                    image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/ieos/${item.image}`,
                                                    start:item.start_date,
                                                    slug:`/ieos/${makeSlug(item.title)}`,
                                                    end:item.end_date,
                                                    description:item.description
                                                    }}
                                                />
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                </div>
                            <div className="col-md-3">

                            </div>
                        </div>
                        <div className="row align-items-center">
                        <div className="col-md-6 mb-5">
                            <div>
                                <nav aria-label="Page navigation example">
                                <ul className="pagination mb-0">
                                    <li className="page-item">
                                    <a className="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                    </li>
                                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                                    <li className="page-item">
                                    <a className="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                    </li>
                                </ul>
                                </nav>
                            </div>
                        </div>
                        <div className="col-md-6 mb-5">
                            <div className="text-end">
                                <a href="#"><img className="" src="/images/Mask_Group18.png"/></a>
                            </div>
                        </div>
                    </div>
                    </div>
                </main>
            </Layout>
        </>
    )
}

export default index
