import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
// import '../node_modules/font-awesome/css/font-awesome.css'
import '../styles/style.css'
import Router from 'next/router';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress


// import App from 'next/app';
// import {Provider} from 'react-redux';
// import withRedus from 'react-redux-wrapper';
// import React from 'react';


//Binding events. 
Router.events.on('routeChangeStart', () => NProgress.start()); Router.events.on('routeChangeComplete', () => NProgress.done()); Router.events.on('routeChangeError', () => NProgress.done());
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
