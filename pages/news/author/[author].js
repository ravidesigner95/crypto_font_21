import Head from 'next/head'
import {useState, useEffect} from "react";
import Link from 'next/link';
import Layout from '../../../components/Layout';
import SmNews from '../../../components/news/smNews';
import Image from 'next/image';
import { replaceImgUrl } from '../../../helpers/GeneralHelpers';


export async function getServerSideProps(context) { 
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/authorInfo?slug=${context.params.author}`);
    const data = await res.json();
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}

export default function author({data}) {
    const [authorDetails, setauthorDetails] = useState(data);
    useEffect(() => {
       console.log(authorDetails);
    });
    return (<>
        <Head>
            <title>Cryptoknowmics - Author</title>
        </Head>
        <Layout breakingNews={
                authorDetails[0]
            }
            token={
                authorDetails[1]
        }>
            <main className="author_detail">
                <section className="pt-2 pb-5">
                    <div className="container-fluid px-5">
                        <div className="p-4 py-5 author_detail_box rounded inShadowBox mb-5">
                            <div className="row">
                                <div className="col-md-2">
                                    <div>
                                    <Image src={
                                                        replaceImgUrl(authorDetails[2][0].guid)
                                                    }
                                                    alt={
                                                        authorDetails[2][0].nickname
                                                    }
                                                    layout="responsive"
                                                    width={200}
                                                    height={200}
                                                    webp="true"/>
                                    </div>
                                </div>
                                <div className="col-md-10">
                                    <div>
                                        <h5 className="mb-3">
                                            <strong className="bcolor">Author : </strong>
                                            <Link href={authorDetails[2][0].nickname}>
                                            <a className="wcolor" >
                                                {authorDetails[2][0].first_name+' '+authorDetails[2][0].lastname}</a>
                                            </Link>
                                            
                                        </h5>
                                        <p>{authorDetails[2][0].description}</p>
                                        <div className="social">
                                            <a href={authorDetails[2][0].facebook != ''?authorDetails[2][0].facebook:'#'} className="fab fa-facebook-f"></a>
                                            <a href={authorDetails[2][0].linkedin != ''?authorDetails[2][0].linkedin:'#'} className="fab fa-linkedin-in"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center">
                            <div className="col-md-7 mb-4">
                                <h4 className="wcolor">Learn Form Our Experts</h4>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                            </div>
                            <div className="col-md-5 mb-4">
                                <div>
                                    <a href="#"><img className="w-100" src="/images/Mask_Group18.png"/></a>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            {authorDetails[3].map((item, i) => (
                                <SmNews type='4' news={item} key={i} getDet={``}/>
                            ))}                
                        </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div>
                                        <nav aria-label="Page navigation example">
                                            <ul className="pagination">
                                                <li className="page-item">
                                                    <a className="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                                <li className="page-item active">
                                                    <a className="page-link" href="#">1</a>
                                                </li>
                                                <li className="page-item">
                                                    <a className="page-link" href="#">2</a>
                                                </li>
                                                <li className="page-item">
                                                    <a className="page-link" href="#">3</a>
                                                </li>
                                                <li className="page-item">
                                                    <a className="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="text-end">
                                        <a href="#"><img className="w-75" src="/images/Mask_Group18.png"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </Layout>
        </>
        )
                }