import Head from 'next/head'
import {useState, useEffect} from "react";
import Link from 'next/link';
import Layout from '../../../components/Layout';
import Image from 'next/image';
import SmTeam from '../../../components/team/SmTeam';
import { replaceImgUrl } from '../../../helpers/GeneralHelpers';

export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/authorList`)
    const data = await res.json()
    // console.log(data[0]);
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
export default function index({data}) {
    const [authors, setAuthors] = useState(data);
    useEffect(() => {
        // console.log(authorDetails);
     });
    return (
        <>
            <Head>
                <title>Our Author - Cryptoknowmics</title>
            </Head>
            <Layout breakingNews={
                    authors[0]
                }
                token={
                    authors[1]
            }>
                <main className="our_Team">
                    <section className="pt-2 pb-5">
                        <div className="container-fluid px-5">
                            <div className="innerBanner p-3 rounded inShadowBox mb-4">
                                <div className="overflow-hidden position-relative h-100 rounded">
                                    <img className="rounded" src="/images/slider1.jpg"/>
                                    <div className="content text-uppercase">
                                        <h2 className="pb-4 text-white">Authors
                                        </h2>
                                        <span className="text-white">
                                            <a className="bcolor" href="#">Home</a>
                                            /Authors</span>
                                    </div>
                                </div>
                            </div>
                            <div className="row info_box">

                                <div className="col-md-7">
                                    <div className="leftBox">
                                        <h4 className="mb-3">
                                            <a href="#">The Story Of Crytoknowmics</a>
                                        </h4>
                                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a
                                            <a href="#" className="bcolor">passage of Lorem Ipsum</a>, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <div>
                                            <a href="#" className="btn crclBtn">Get a Free Proposal</a>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-md-5">
                                    <div className="rightBox">
                                        <div className="post_img mb-4">
                                            <img className="rounded w-100 cat_img" src="/images/slider1.jpg"/>
                                        </div>
                                        <div className="add mb-5">
                                            <img className="w-100" src="/images/NoPath.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="py-5 text-center educatores">
                            <div className="container px-5">
                                <h4 className="mb-4 text-white">We're A Close-Knit Team Of Enthusiastic marketers, Strategists, Creators, And Educatores. Think You'll Fit In Just Fine</h4>
                                <p className="text-center mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                <a href="#" className="btn crclBtn">Join Our Team</a>
                            </div>
                        </div>
                        <div className="container-fluid px-5">
                            <div className="row align-items-center py-5">
                                <div className="col-md-7 mb-4">
                                    <h4 className="wcolor">Meet Our Team & Advisors</h4>
                                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                                </div>
                                <div className="col-md-5 mb-4">
                                    <div>
                                        <a href="#"><img className="w-100" src="/images/Mask_Group18.png"/></a>
                                    </div>
                                </div>
                            </div>
                            <div className="our_team_section">
                                <div className="our_team_body d-flex flex-wrap">
                                    {
                                    authors[2].map((item, i) => (
                                        <SmTeam key={i} item={{name:item.first_name+' '+item.lastname, description:item.description, image:replaceImgUrl(item.guid), position:"", slug:item.nickname}}/>
                                    ))
                                } </div>
                            </div>
                            <div className="row mt-5">
                                <div className="col-md-5">
                                    <a href="#"><img className="w-100" src="/images/top_add.png"/></a>
                                </div>
                                <div className="col-md-5 offset-md-2">
                                    <a href="#"><img className="w-100" src="/images/Mask_Group18.png"/></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </Layout>
        </>
    )
}