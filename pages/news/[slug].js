import Head from 'next/head'
import {useState, useEffect} from "react";
import Link from 'next/link';
import Layout from '../../components/Layout';
import Image from 'next/image';
import ReactHtmlParser from 'react-html-parser';
import {replaceHtml, replaceImgUrl, newsUrl, dateFormat} from '../../helpers/GeneralHelpers';
import NewsDetails from '../../components/news/NewsDetails';
import NewsCategory from '../../components/news/NewsCategory';
import NewsTag from '../../components/news/NewsTag';

export async function getServerSideProps(context) {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/news-details?slug=${
        context.params.slug
    }`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}

export default function slug({data}) {
    const [newsDetails, setnewsDetails] = useState(data);
    // let newsContent = newsDetails[2][0].post_content;
    // newsContent = newsContent.replace(/src=\"(.+?)\"/g, function (m, p1) {
    //     return `src="${replaceImgUrl(p1)}"`;;
    // });
    // newsContent = newsContent.split("\r\n");
    useEffect(() => {
        console.log(newsDetails);
    })
    let getDetails = async(e) => {
        e.preventDefault();
        console.log(e.target.getAttribute('slug'));
        let slug = e.target.getAttribute('slug');
        const res = await fetch(`http://cryptoknowmics.com/ck/v4/news-details?slug=${slug}`)
        const data = await res.json()
        if (! data) {
            return {notFound: true}
        }
        setnewsDetails(data);
    }
    return (
        <>
            <Head> {
                (() => {
                    if (newsDetails[2][0].post_details == 'post_details') {
                        return (
                            <>
                                <title>{
                                    // newsDetails[3][0].post_title
                                }</title>
                                <meta name="description"
                                    // content={newsDetails[4][0].meta_description}
                                    >

                                    </meta>
                            </>
                        )
                    } else if (newsDetails[2][0].category_details == 'category_details') {
                        return (
                            <>
                                <title>category</title>
                                <meta name="description" content=""></meta>
                            </>
                        )
                    } else {
                        return (
                            <>
                                <title>tag</title>
                                <meta name="description" content=""></meta>
                            </>
                        )
                    }
                })()
            } </Head>
            <Layout breakingNews={
                    newsDetails[0]
                }
                token={
                    newsDetails[1]
            }>
                {
                (() => {
                    if (newsDetails[2][0].post_details == 'post_details') {
                        return (
                            <NewsDetails details={newsDetails}
                                getDet={getDetails}/>
                        )
                    } else if (newsDetails[2][0].category_details == 'category_details') {
                        return (
                            // <h1>Category</h1>
                            <NewsCategory category={newsDetails}
                                getDet={getDetails}/>
                        )
                    } else{
                        return(
                            <h1>Tag</h1>
                            // <NewsTag category={newsDetails} getDet={getDetails}/>
                        )
                    }
                })()
            } </Layout>
        </>
    )
}