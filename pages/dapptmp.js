import React from "react";
import Image from "next/image";
import Link from "next/link";

const dapptmp = () => {
  return (
    <main>
      <div className="container-fluid px-5">
        <div className="p-4 py-5 rounded inShadowBox mb-4">
          <div className="row align-items-center">
            <div className="col-md-6">
              <div className="row">
                <div className="col-md-3">
                  <div>
                    <Image
                      src="/images/img1.jpg"
                      height={150}
                      width={150}
                      webp="ture"
                      className="rounded2 obj_fit_cover"
                    />
                  </div>
                </div>
                <div className="col-md-9">
                  <div>
                    <h5 className="dwcolor">Blockchain Expo Global 2020</h5>
                    <div className="d-flex flex-column lgcolor">
                      <span className="wcolor mb-2">
                        <strong className="bcolor">Location :</strong> Kerry
                        Hotal, Hong Kong
                      </span>
                      <span>Start : 22nd Mar 2021</span>
                      <span>End : 23nd Mar 2021</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="text-end mb-5">
                <a href="#" className="crclBtn d-inline-block">
                  Visit Website
                </a>
              </div>
              <div className="circle_small_social text-end">
                <a href="#" className="fab fa-facebook-f"></a>
                <a href="#" className="fab fa-twitter"></a>
                <a href="#" className="fab fa-telegram-plane"></a>
                <a href="#" className="fab fa-pinterest-p"></a>
                <a href="#" className="fab fa-linkedin-in"></a>
                <a href="#" className="fab fa-youtube"></a>
                <a href="#" className="fab fa-instagram"></a>
                <a href="#" className="fas fa-rss"></a>
                <a href="#" className="fab fa-reddit-alien"></a>
              </div>
            </div>
          </div>
        </div>
       
        <div className="p-4 outShadowBox rounded mb-5">
          <div className="row">
            <div className="col-md-6 mb-5">
              <h5 className="dwcolor">Leave a Comments</h5>
              <p>
                Contrary to popular belief, Lorem Ipsum is not simply random
                text. It has roots in a piece of classical Latin literature from
                45 BC
              </p>
              <form>
                <div className="mb-4">
                  <textarea
                    className="form-control inShadowBox rounded p-3 bg-transparent border-0 lgcolor"
                    placeholder="add a comment..."
                    rows="4"
                  ></textarea>
                </div>
                <div>
                  <button className="crclBtn border-0">Submit</button>
                </div>
              </form>
            </div>
            <div className="col-md-6 mb-5">
              <div className="d-flex aling-items-center justify-content-center my-4">
                <strong className="bcolor">Share On :</strong>
                <div className="circle_small_social text-end ms-4">
                  <a href="#" className="fab fa-facebook-f"></a>
                  <a href="#" className="fab fa-twitter"></a>
                  <a href="#" className="fab fa-telegram-plane"></a>
                  <a href="#" className="fab fa-pinterest-p"></a>
                  <a href="#" className="fab fa-linkedin-in"></a>
                  <a href="#" className="fab fa-youtube"></a>
                  <a href="#" className="fab fa-instagram"></a>
                  <a href="#" className="fas fa-rss"></a>
                  <a href="#" className="fab fa-reddit-alien"></a>
                </div>
              </div>
              <div className="mb-4">
                <h5 className="dwcolor">Enter Shared Links</h5>
                <p>
                  Contrary to popular belief, Lorem Ipsum is not simply random
                  text.
                </p>
                <div className="inShadowBox rounded p-3 d-flex">
                  <input
                    type="text"
                    className="form-control bg-transparent border-0 shadow-none lgcolor"
                    placeholder="Ex@https://facebook.com/@youid/sharedlink"
                  />
                  <input
                    type="submit"
                    value="Send"
                    className="crclBtn rounded border-0"
                  />
                </div>
              </div>
              <div className="mb-4 text-end">
                <div>
                  <a href="#">
                    <img className="w-75" src="/images/Mask_Group18.png" />
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="comment">
            <h4 className="mb-4 dwcolor">Recents Comments</h4>
            {[0, 1, 2, 3, 4, 5, 6].map((i) => (
              <div key={i} className="commentBox rounded inShadowBox p-4 mb-4">
                <div className="commentHead mb-3">
                  <div className="d-flex align-items-center">
                    <Link href="#">
                      <a>
                        <Image
                          src="/images/img1.jpg"
                          width={40}
                          height={40}
                          webp="true"
                          className="rounded-circle"
                        />
                      </a>
                    </Link>
                    <div className="ms-3 d-flex flex-column">
                      <Link href="#">
                        <a>@Nimisha</a>
                      </Link>
                      <small className="lgcolor">5 days ago</small>
                    </div>
                  </div>
                </div>
                <div className="body">
                  <p className="m-0">
                    There are many variations of passages of Lorem Ipsum
                    available, but the majority have suffered alteration in some
                    form, by injected humour, or randomised words which don't
                    look even slightly believable. If you are going to use a
                    passage of Lorem Ipsum, you need to be sure there isn't
                    anything embarrassing hidden in the middle of text.
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="row align-items-center">
          <div className="col-md-7 mb-5">
            <h4 className="wcolor">Popular Events</h4>
            <p>
              Contrary to popular belief, Lorem Ipsum is not simply random text.
              It has roots in a piece of classical Latin literature from 45 BC,
              making it over 2000 years old. It uses a dictionary of over 200
              Latin words, combined with a handful of model sentence structures,
              to generate Lorem Ipsum which looks reasonable.
            </p>
          </div>
          <div className="col-md-5 mb-5">
            <div>
              <a href="#">
                <img className="w-100" src="/images/Mask_Group18.png" />
              </a>
            </div>
          </div>
        </div>
        <div className="p-4 outShadowBox rounded mb-5">
            <div className="row">
                {
                    [0,1,2,3,4,5,6].map((i)=>(
                        <div key={i} className="col-md-6 mb-4">
                            <div className="box inShadowBox rounded p-3">
                                    {i<3 ?
                                        <span className={`sticker text-uppercase position-absolute ${i==1 ? 'orange':'blue'}`}>Featured</span>
                                        :
                                        null
                                    }
                                <div className="row">
                                    <div className="col-md-3 pe-0">
                                        <Link href="#">
                                            <a className="d-block">
                                                <Image 
                                                    src="/images/img1.jpg"
                                                    height={150}
                                                    width={150}
                                                    webp="true"
                                                    className="rounded2 obj_fit_cover"
                                                />
                                            </a>
                                        </Link>
                                    </div>
                                    <div className="col-md-9 ps-4 ps-md-0">
                                        <h6>
                                            <Link href="#">
                                                <a>Blockchain Expo Global 2020</a>
                                            </Link>
                                        </h6>
                                        <div className="d-flex flex-column mb-3 lgcolor">
                                            <span>Token : Digimon</span>
                                            <span>Start : 17th Mar 2021</span>
                                            <span>End : 18th Mar 2021</span>
                                        </div>
                                        <div className="circle_small_social">
                                            <a href="#" className="fab fa-facebook-f"></a>
                                            <a href="#" className="fab fa-twitter"></a>
                                            <a href="#" className="fab fa-telegram-plane"></a>
                                            <a href="#" className="fab fa-pinterest-p"></a>
                                            <a href="#" className="fab fa-linkedin-in"></a>
                                            <a href="#" className="fab fa-youtube"></a>
                                            <a href="#" className="fab fa-instagram"></a>
                                            <a href="#" className="fas fa-rss"></a>
                                            <a href="#" className="fab fa-reddit-alien"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
      </div>
    </main>
  );
};

export default dapptmp;
