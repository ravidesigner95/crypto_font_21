import React from 'react'
import FeatureCon from '../../components/includes/FeatureCon';
import Layout from '../../components/Layout'
import { useState, useEffect } from "react";
import Link from 'next/link';
import Image from 'next/image';
import SmCon from '../../components/includes/SmCon';
import AboutCon from '../../components/includes/AboutCon';


export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/events`);
    const data = await res.json();
    // console.log(data[0]);
    if (!data) {
      return { notFound: true };
    }
    return {
      props: {
        data,
      },
    };
  }
const event = ({data}) => {
    const [events, setevents] = useState(data);
    useEffect(() => {
    console.log(events)
    });
    return (
        <Layout breakingNews={events[0]} token={events[1]}>
            <main className="all_events">
                <section className="pt-2 pb-5">
                    <div className="container-fluid px-5">
                    <FeatureCon image="/images/slider1.jpg" title="Events"/>
                        <AboutCon/>
                        <div className="p-4 outShadowBox rounded mb-5">
                            
                            <div className="tabs mb-4">
                                <a href="#" className="active">All Events</a>
                                <a href="#">Conference</a>
                                <a href="#">Coin</a>
                            </div>

                            <div className="row">
                                {events[2].map((item, i) => (
                                     <SmCon key={i} item={{type:'event', title:item.event_name, featured:item.featured, latest:item.featured, premium:item.featured, slug:`events/${item.event_name}`, image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/event_images/${item.event_image}`, event:{location:item.event_location, start:item.event_start_date, end:item.event_end_date} }}/>
                                ))}
                            </div>
                            
                        </div>
                    </div>
                </section>
            </main>
         </Layout>
    )
}

export default event
