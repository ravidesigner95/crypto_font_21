import Head from 'next/head'
import React from 'react'
import FeatureCon from '../../components/includes/FeatureCon';
import Layout from '../../components/Layout'
import {useState, useEffect} from "react";
import Link from 'next/link';
import Image from 'next/image';
import SmCon from '../../components/includes/SmCon';
import AboutCon from '../../components/includes/AboutCon';
import LgCon from '../../components/includes/LgCon';
import Comment from '../../components/includes/Comment';
import RelatedCon from '../../components/includes/RelatedCon';

export async function getServerSideProps(context) {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/events-details?slug=${
        context.params.slug
    }`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
const eventDetails = ({data}) => {
    const [event, setevent] = useState(data);
    useEffect(() => {
        console.log(event)
    });
    return (
        <>
            <Head>
                <title>Events Details</title>
            </Head>
            <Layout breakingNews={
                    event[0]
                }
                token={
                    event[1]
            }>
                <main>
                    <div className="container-fluid px-5">
                       <LgCon item={{type:'event', title:event[2][0].event_name, featured:'', latest:'', premium:'', image:`${event[2][0].url}${event[2][0].guid}`, data:{location:event[2][0].event_location, start:event[2][0].event_start_date, end:event[2][0].event_end_date, website:event[2][0].event_website, ticket:event[2][0].event_ticket, description:event[2][0].event_descrioption} }}/>
                      <Comment/>
                      <RelatedCon/>
                    </div>
                </main>
            </Layout>
        </>
    )
}

export default eventDetails