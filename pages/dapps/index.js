import Head from 'next/head'
import {useState, useEffect} from "react";
import React from 'react';
import Image from 'next/image';
import Link from 'next/link'; 
import Layout from '../../components/Layout';
import SmCon from '../../components/includes/SmCon';

export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/dapps`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
const dapps = ({data}) => {
    const [dapps, setdapps] = useState(data);
    useEffect(() => {
     console.log(dapps);  
    });
    return (
        <>
        <Head>
            <title>Dapps - Cryptoknowmics</title>
        </Head>
        <Layout breakingNews={
                    dapps[0]
                }
                token={
                    dapps[1]
            }>
            
        <main className="dapps">
            <section className="pt-2 pb-5">
                <div className="container-fluid px-5">
                    <div className="p-4 py-5 title_info_box rounded inShadowBox mb-5">
                        <div className="row">
                            <div className="col-md-2">
                                <div>
                                    <Image
                                        src="/images/Jorge_sebastiao.jpg"
                                        width={200}
                                        height={200}
                                        webp={true}
                                        className="rounded obj_fit_cover"
                                    />
                                </div>
                            </div>
                            <div className="col-md-10">
                                <div>
                                    <h5 className="mb-3 wcolor"><span className="bcolor">Decentralized</span> Applications (DApps)</h5>
                                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classNameical literature, discovered the undoubtable source simply random text. It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classNameical literature, discovered the undoubtable source.</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                        <div className="row align-items-center">
                            <div className="col-md-7 mb-4">
                                <h4 className="wcolor">Learn Form Our Experts</h4>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                            </div>
                            <div className="col-md-5 mb-4">
                                <div>
                                    <a href="#"><img className="w-100" src="images/Mask_Group18.png" /></a>
                                </div>
                            </div>
                        </div>
                        <div className="p-4 outShadowBox rounded mb-5">
                            <div className="row">
                              {dapps[2].map((item, i) => (
                                   <SmCon key={i} item={{type:'dapps', title:item.name, featured:item.featured, latest:item.featured, premium:item.featured, slug:`dapps/${item.name}`, image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/dapps/${item.image}`, dapps:{description:item.description} }}/>
                              ))}
                            </div>
                        </div>
                        <div className="p-4 outShadowBox rounded mb-5">
                            <div className="row">
                                
                            </div>
                        </div>
                </div>
            </section>
        </main>
        </Layout>
        </>
    )
}

export default dapps
