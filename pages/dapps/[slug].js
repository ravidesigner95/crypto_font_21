import Head from 'next/head'
import {useState, useEffect} from "react";
import React from 'react';
import Image from 'next/image';
import Link from 'next/link'; 
import Layout from '../../components/Layout';
import LgCon from '../../components/includes/LgCon';
import Comment from '../../components/includes/Comment';
import RelatedCon from '../../components/includes/RelatedCon';



export async function getServerSideProps(context) {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/dapps-details?slug=${
        context.params.slug
    }`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
export default function DappsDetails({data}) {
    const [dapps, setdapps] = useState(data);
    useEffect(() => {
        console.log(dapps)
    });
    return (
        <>
            <Head>
                <title>Events Details</title>
            </Head>
            <Layout breakingNews={
                    dapps[0]
                }
                token={
                    dapps[1]
            }>
                <main>
                <div className="container-fluid px-5">
                       <LgCon item={{type:'dapps', title:dapps[2][0].name, featured:'', latest:'', premium:'',image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/dapps/${dapps[2][0].logo}`, data:{ website:dapps[2][0].website_link, description:dapps[2][0].description, value:dapps[2][0].value, location:"", token:dapps[2][0].symbol, network:dapps[2][0].network_type, category:dapps[2][0].category } }}/>
                      <Comment/>
                      <RelatedCon/>
                    </div>
                </main>
            </Layout>
        </>
    )
}