import Head from 'next/head'
import {useState, useEffect} from "react";
import Link from 'next/link';
import Layout from '../components/Layout';
import Image from 'next/image';
import SmTeam from '../components/team/SmTeam';
import FeatureCon from '../components/includes/FeatureCon';

export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/ourPartner`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
export default function partners({data}){
    const [partners, setPartners] = useState(data);
    useEffect(() => {
        console.log(partners)
    });
    return(
        <>
        <Head>
            <title>Our Partners</title>
        </Head>
        <Layout breakingNews={
                    partners[0]
                }
                token={
                    partners[1]
            }>
        <h1>partners</h1>
        </Layout>
        </>
    )
}