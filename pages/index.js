import Head from 'next/head'
import {useState, useEffect} from "react";
import Link from 'next/link';
import Layout from '../components/Layout';
import Image from 'next/image';
import SmNews from '../components/news/smNews';
import ShowLink from '../components/news/ShowLink';
import Heading from '../components/news/Heading';
import Sidebar from '../components/sidebar/Sidebar';
import {replaceHtml, replaceImgUrl, newsUrl} from '../helpers/GeneralHelpers';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from "react-responsive-carousel";

export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/landing`)
    const data = await res.json()
    // console.log(data[0]);
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}

export default function Home({data}) {
    const [homeData, sethomeData] = useState(data);
    useEffect(() => {
        console.log(homeData)
    })
    return (
        <>
            <Head>
                <title>Breaking Crypto News | Altcoin News Today | Cryptoknowmics</title>
                <link rel="icon" href="https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/logo/favicon.ico"/>
            </Head>
            <Layout breakingNews={
                    homeData[0]
                }
                token={
                    homeData[1]
            }>
                <main>
                    <section className="py-4">
                        <div className="container-fluid px-5">
                            <div className="row">
                                <div className="col-md-9">
                                    <div className="leftBox">
                                        <div className="slider_section section">
                                            <div className="row">
                                                <SmNews type='1'
                                                    news={
                                                        homeData[5][0]
                                                    }
                                                    keyVal='1'/>
                                                <div className="col-md-8 mb-5">
                                                    <div className="p-3 outShadowBox rounded">
                                                        <div id="topSlider" className="carousel slide">
                                                            <Carousel showThumbs={false}
                                                                showStatus={false}
                                                                showIndicators={false}
                                                                renderArrowPrev={
                                                                    (onClickHandler, hasPrev, label) => hasPrev && (
                                                                        <button type="button"
                                                                            onClick={onClickHandler}
                                                                            className="carousel-control-prev"
                                                                            title={label}>
                                                                            <i className="fas fa-arrow-left carousel-control-prev-icon inShadowBox"></i>
                                                                        </button>
                                                                    )
                                                                }
                                                                renderArrowNext={
                                                                    (onClickHandler, hasNext, label) => hasNext && (
                                                                        <button type="button"
                                                                            onClick={onClickHandler}
                                                                            className="carousel-control-next"
                                                                            title={label}>
                                                                            <i className="fas fa-arrow-right carousel-control-next-icon inShadowBox"></i>
                                                                        </button>
                                                                    )
                                                            }>

                                                                {
                                                                homeData[2].map((item, i) => (
                                                                    <div key={i}
                                                                        className="position-relative rounded-2 overflow-hidden">
                                                                        <Link href={
                                                                            newsUrl(item.slug)
                                                                        }>
                                                                            <a>
                                                                                <Image src={
                                                                                        replaceImgUrl(item.image)
                                                                                    }
                                                                                    alt={
                                                                                        item.post_title
                                                                                    }
                                                                                    layout="responsive"
                                                                                    width={750}
                                                                                    height={468}
                                                                                    webp="true"
                                                                                    className="rounded-2"
                                                                                    />
                                                                                <div className="legend carousel-caption">
                                                                                    <h3>

                                                                                        <a className="text-white text-decoration-none title">
                                                                                            {
                                                                                            item.post_title
                                                                                        } </a>
                                                                                    </h3>
                                                                                    <p className="m-0">
                                                                                        {
                                                                                        `${item.post_content.length > 170 ? replaceHtml(item.post_content,170)+' ...' : replaceHtml(item.post_content)}`  
                                                                                    }</p>
                                                                                </div>
                                                                            </a>
                                                                        </Link>
                                                                    </div>
                                                                ))
                                                            } </Carousel>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="section">
                                            <div className="row">
                                                {
                                                homeData[2].map((item, i) => ((i > 4 && i < 8) && <SmNews type='1'
                                                    news={item}
                                                    key={i}/>))
                                            } </div>
                                        </div>
                                        <ShowLink url="/news/news" adurl="#" adImage="/images/NoPath.png"/>
                                        <div className="top_picks section">
                                            <Heading type='1'
                                                heading={
                                                    {
                                                        title: "Top Picks",
                                                        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
                                                    }
                                                }/>
                                            <div className="row">
                                                {
                                                homeData[3].map((item, i) => (
                                                    <SmNews type='1'
                                                        news={item}
                                                        key={i}/>
                                                ))
                                            } </div>
                                        </div>
                                        <ShowLink url="/news/top-picks" adurl="#" adImage="/images/NoPath.png"/>
                                        <div className="bitcoin_news section">
                                            <Heading type='1'
                                                heading={
                                                    {
                                                        title: "Technical Analysis",
                                                        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
                                                    }
                                                }/>
                                            <div className="row">
                                                <div className="col-md-8 mb-5">
                                                    <div className="box p-3 inShadowBox outShadowBox_after position-relative rounded">
                                                        <div className="list pe-4">
                                                            <ul className="m-0 p-0 list-unstyled">
                                                                {
                                                                homeData[4].map((item, i) => ((i < 7) && <SmNews type='2'
                                                                    news={item}
                                                                    key={i}/>))
                                                            } </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <SmNews type='1'
                                                    news={
                                                        homeData[4][7]
                                                    }/>
                                            </div>
                                        </div>
                                        <ShowLink url="/news/technical-analysis" adurl="#" adImage="/images/NoPath.png"/>
                                        <div className="section mb-5">
                                            <Heading type='1'
                                                heading={
                                                    {
                                                        title: "Bitcoin News",
                                                        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
                                                    }
                                                }/>
                                            <div className="row">
                                                {
                                                homeData[5].map((item, i) => ((i == 1) ? <SmNews type='3'
                                                    news={item}
                                                    key={i}/> : <SmNews type='1'
                                                    news={item}
                                                    key={i}/>))
                                            } </div>
                                        </div>
                                        <ShowLink url="/news/bitcoin-news" adurl="#" adImage="/images/NoPath.png"/>
                                        <div className="section mb-5">
                                            <Heading type='1'
                                                heading={
                                                    {
                                                        title: "Alt Coin News",
                                                        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
                                                    }
                                                }/>
                                            <div className="row">
                                                {
                                                homeData[10].map((item, i) => ((i == 1) ? <SmNews type='3'
                                                    news={item}
                                                    key={i}/> : <SmNews type='1'
                                                    news={item}
                                                    key={i}/>))
                                            } </div>
                                        </div>
                                        <ShowLink url="/news/altcoins" adurl="#" adImage="/images/NoPath.png"/>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="rightBox">
                                        <Sidebar type="currency"/>
                                        <Sidebar type="pressRelease"
                                            sidebar={
                                                {
                                                    showLink: "#",
                                                    showText: "View More"
                                                }
                                            }
                                            data={
                                                homeData[11]
                                            }
                                            data1={
                                                homeData[12]
                                            }/>
                                        <Sidebar type="eventCalender"
                                            sidebar={
                                                {
                                                    showLink: "#",
                                                    showText: "View More"
                                                }
                                            }
                                            data={
                                                homeData[7]
                                            }/>
                                        <Sidebar type="cryptoGuide"
                                            sidebar={
                                                {
                                                    showLink: "#",
                                                    showText: "View More"
                                                }
                                            }
                                            data={
                                                homeData[6]
                                            }/>
                                        <Sidebar type="airdrops"
                                            sidebar={
                                                {
                                                    showLink: "#",
                                                    showText: "View More"
                                                }
                                            }
                                            data={
                                                homeData[8]
                                            }/>
                                        <Sidebar type="ads"
                                            sidebar={
                                                {
                                                    showLink: "#",
                                                    showText: "View More"
                                                }
                                            }/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </Layout>
        </>
    )
}