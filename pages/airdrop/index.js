import {useState, useEffect} from "react";
import FeatureCon from '../../components/includes/FeatureCon';
import Image from 'next/image';
import Link from 'next/link';
import Head from 'next/head'
import Layout from '../../components/Layout';
import SmCon from "../../components/includes/SmCon";
import AboutCon from '../../components/includes/AboutCon';


export async function getStaticProps() {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/airdrops`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
const airdrop = ({data}) => {
    const [airdrops, setairdrops] = useState(data);
    useEffect(() => {
        console.log(airdrops);
    });
    return (
        <>
        <Head>
            <title>Airdrop - cryptoknowmics</title>
        </Head>
        <Layout breakingNews={
                    airdrops[0]
                }
                token={
                    airdrops[1]
            }>
        <main className="all_airdrop">
                <section className="pt-2 pb-5">
                    <div className="container-fluid px-5">
                        <FeatureCon image="/images/slider1.jpg" title="Airdrop"/>
                        <AboutCon/>
                        <div className="row align-items-center">
                            <div className="col-md-7 mb-4">
                                <h4 className="wcolor">Learn Form Our Experts</h4>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classNameical Latin literature from 45 BC, making it over 2000 years old. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>
                            </div>
                            <div className="col-md-5 mb-4">
                                <div>
                                    <a href="#"><img className="w-100" src="images/Mask_Group18.png" /></a>
                                </div>
                            </div>
                        </div>
                        <div className="p-4 outShadowBox rounded mb-5">
                        <div className="row">
                            {airdrops[2].map((item, i) => (
                                <SmCon key={i} item={{type:'airdrop', title:item.name, featured:item.featured, latest:item.featured, premium:item.featured, slug:`airdrop/${item.name}-${item.symbol}`, image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/airdrops/${item.logo}`, airdrop:{location:item.event_location, start:item.start_date, end:item.end_date, token:item.symbol} }}/>
                            ))}                            
                            </div>
                        </div>
                    </div>
            </section>
        </main>
        </Layout>
        </>
    )
}

export default airdrop
