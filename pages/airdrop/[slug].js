import Head from 'next/head'
import {useState, useEffect} from "react";
import React from 'react';
import Image from 'next/image';
import Link from 'next/link'; 
import Layout from '../../components/Layout';
import LgCon from '../../components/includes/LgCon';
import Comment from '../../components/includes/Comment';
import RelatedCon from '../../components/includes/RelatedCon';


export async function getServerSideProps(context) {
    const res = await fetch(`http://cryptoknowmics.com/ck/v4/airdrops-details?slug=${
        context.params.slug
    }`)
    const data = await res.json()
    if (! data) {
        return {notFound: true}
    }

    return {props: {
            data
        }}
}
export default function AirdropDetails({data}) {
    const [airdrop, setairdrop] = useState(data);
    useEffect(() => {
        console.log(airdrop)
    });
    return (
        <>
            <Head>
                <title>Airdrop Details</title>
            </Head>
            <Layout breakingNews={
                    airdrop[0]
                }
                token={
                    airdrop[1]
            }>
                <main>
                    <div className="container-fluid px-5">
                       <LgCon item={{type:'airdrop', title:airdrop[2][0].name, featured:'', latest:'', premium:'',image:`https://s3-ap-southeast-2.amazonaws.com/www.cryptoknowmics.com/airdrops/${airdrop[2][0].logo}`, data:{start:airdrop[2][0].start_date, end:airdrop[2][0].end_date, website:airdrop[2][0].website_link, description:airdrop[2][0].description, value:airdrop[2][0].value, location:"", token:airdrop[2][0].symbol } }}/>
                      <Comment/>
                      <RelatedCon/>
                    </div>
                </main>
            </Layout>
        </>
    )
}